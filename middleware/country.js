
const db = require('../sequelize/connection');

verifycountry = async (req, res, next) =>{

    clientIp = req.headers['x-forwarded-for'] ? req.headers['x-forwarded-for'].split(',')[0] : req.connection.remoteAddress;
    const ipV4 = clientIp.replace(/^.*:/, '')

    try{
        const response = await db.query(`call btcwe.pro_isBlocked('${ipV4}')`);
        if(response[0].isBlocked)
            return res.status(403).json({ 
                message: 'blocked' 
            });


        next();
    } catch (err) {
        return res.status(500).json({ 
            message: err 
        });
    }

}

module.exports = verifycountry;