const jwt = require('jsonwebtoken');
const config = require('../config/config');

verifyToken = (req, res, next) =>{
    let token = req.headers['x-auth-token'];

    if (!token){
        return res.status(203).json({ 
            message: 'Fail to Authentication. Error -> ' 
        });
    }

    jwt.verify(token, config.affSecretToken, (err, decoded) => {
        if (err){
            return res.status(403).json({ 
                    message: 'Fail to Authentication. Error -> ' + err 
                });
        }
        
        req.aff_id = decoded.aff_id;
        req.aff_name = decoded.aff_name;
        next();
    });
}

const authJwt = {};
authJwt.verifyToken = verifyToken;

module.exports = authJwt;