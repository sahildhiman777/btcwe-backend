const jwt = require('jsonwebtoken');
const config = require('../config/config');

function auth(req,res,next){
    const token = req.header('x-auth-token');
    if(!token){
        // console.log('Error no token');
        return res.status(200).json({
            code : 599,
            message : 'Access denied. No token found.',
            data : []
        });
        // next();
    }
    else{
        try{
            const decoded = jwt.verify(token, config.secretToken);
            if(req.body.email){
                req.body.email = decoded.email;
                console.log(req.body.email, 'auth req body');
                
            }
            if(req.query.email){
                req.query.email = decoded.email;
                console.log(req.query.email, 'auth req query');
            }
            next();
        }
        catch(err){
            console.log(err, 'Error session out');
            return res.status(200).json({
                code : 599,
                message : 'Session timedout.',
                data : []
            });
        }
    }
}
module.exports = auth;
