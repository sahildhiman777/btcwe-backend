const jwt = require('jsonwebtoken');
const config = require('../config/config');

verifyToken = (req, res, next) =>{
    let token = req.headers['x-auth-token'];
    if (!token){
        return res.status(203).json({ 
            message: 'Fail to Authentication. Error -> ' 
        });
    }

    jwt.verify(token, config.secretToken, (err, decoded) => {
        if (err){
            return res.status(403).json({ 
                    message: 'Fail to Authentication. Error -> ' + err 
                });
        }
        
        req.agent = decoded.ag_id;
        req.isAdminOrManager = (decoded.group_id !== 1 && decoded.group_id !== 2)? false:true;
        next();
    });
}

isAdmin = (req, res, next) => {
    let token = req.headers['x-auth-token'];
    if (!token){
        return res.status(203).json({ 
            message: 'Fail to Authentication. Error -> ' 
        });
    }

    jwt.verify(token, config.secretToken, (err, decoded) => {
        if (err){
            return res.status(403).json({ 
                    message: 'Fail to Authentication. Error -> ' + err 
                });
        }

        if(decoded.group_id != 1){ // admin == group id 1
            return res.status(403).json({ 
                message: 'Require Admin Role!'
            });
        }
        next();
    });
}

isManagerOrAdmin = (req, res, next) => {
    let token = req.headers['x-auth-token'];
    if (!token){
        return res.status(203).json({ 
            message: 'Fail to Authentication. Error -> ' 
        });
    }

    jwt.verify(token, config.secretToken, (err, decoded) => {
        if (err){
            return res.status(403).json({ 
                    message: 'Fail to Authentication. Error -> ' + err 
                });
        }

        if(decoded.group_id != 1 && decoded.group_id != 2){ // admin = group id 1 , manager = group id 2
            return res.status(403).json({ 
                message: 'Require Admin or manager Role!'
            });
        }
        next();
    });
}

const authJwt = {};
authJwt.verifyToken = verifyToken;
authJwt.isAdmin = isAdmin;
authJwt.isManagerOrAdmin = isManagerOrAdmin;

module.exports = authJwt;

