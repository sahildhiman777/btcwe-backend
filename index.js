const express = require('express');
const bodyParser = require('body-parser');
const http = require('http');
const https = require('https');
const fs = require('fs');
const path = require('path');
const route = require('./routes');
const db = require('./sequelize/connection');
const socket = require('./real-time-rates/socket')
const realTimeRates = require('./real-time-rates')



// create express app
const app = express();
const port = process.env.port || 4000;

// var certificate = fs.readFileSync('/etc/ssl/btcwe/btcwe_io.crt', 'utf8');
// var privateKey = fs.readFileSync('/etc/ssl/btcwe/btcwe_io.key', 'utf8');
// var caBundle = fs.readFileSync('/etc/ssl/btcwe/btcwe_io.ca-bundle', 'utf8');

// var credentials = { key: privateKey, cert: certificate, ca: caBundle };

var httpServer = http.createServer(app);
// var httpServer = https.createServer(credentials, app);

// parse requests of content-type - application/x-www-form-urlencoded
app.use(bodyParser.urlencoded({ extended: true }))

// parse requests of content-type - application/json
app.use(bodyParser.json());

app.use(express.static(path.join(__dirname, '/public')));

// app.use(function (req, res, next) {
//     res.header("Access-Control-Allow-Origin", "*");
//     res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept, x-auth-token");
//     next();
// });
app.use(function (req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header(
    "Access-Control-Allow-Methods",
    "GET,HEAD,PUT,POST,OPTIONS,UPDATE,DELETE"
    );
    res.header(
    "Access-Control-Allow-Headers",
    "Origin, X-Requested-With, Content-Type, Accept, Authorization, token, x-auth-token"
    );
    next();
});

app.use('/api', route)

// catch 404 and forward to error handler
app.use(function (req, res, next) {
    var err = new Error('Not Found');
    err.status = 404;
    next(err);
});

// error handler
app.use(function (err, req, res, next) {
    res.status(err.status || 500);
    res.json(err);
});


// listen for requests
httpServer.listen(port, function () {
    console.log('api is listening on port' + port);
});

socket.connect(httpServer)

realTimeRates.start();
