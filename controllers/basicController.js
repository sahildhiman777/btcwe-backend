const db = require('../sequelize/connection');
const bcrypt = require('bcrypt');
const saltRounds = 10;
var jwt = require('jsonwebtoken');
const config = require('../config/config.js');

exports.authenticate = async function (req, res) {

    const { email, password } = req.body,
        clientIp = req.headers['x-forwarded-for'] ? req.headers['x-forwarded-for'].split(',')[0] : req.connection.remoteAddress;
        // var salt = bcrypt.genSaltSync(saltRounds);
        // var hash = bcrypt.hashSync(password, salt);

    try {
        const login = await db.query(
            `CALL btcwe.v2_platform_get_client_data(
                    '${email}'
                )`
        );


        const response = login[0].object;
 
        const real_password = response.password;

        var token = jwt.sign({ email: email }, config.secretToken);
        if (response === '') {
            res.json({
                status: "error",
                message: "Client not found!",
            });
            return;
        }
        // if (response.password !== hash) {
        let hash = real_password.replace(/^\$2y(.+)$/i, '$2a$1');
        bcrypt.compare(password, hash, function(er, re){
            if(er){
                return res.json({
                    status: "error",
                    message: "Invalid credentials please verify before login."
                });
            }
            if(re) {
                return res.json({
                    status: "success",
                    message: "Logged in successfully.",
                    token:token,
                    userInfo:response
                });
            }else{
                return res.json({
                    status: "error",
                    message: "Invalid credentials please verify before login."
                });
            }
        })
        
    } catch (err) {
        res.json({
            status: "error",
            message: "Invalid credentials please verify before login."
            });
        return;
    }
}

exports.register = async function (req, res) {


    const { email, password , first_name,last_name,contact,country_name,campaign,token,param1,param2} = req.body,
    clientIp = req.headers['x-forwarded-for'] ? req.headers['x-forwarded-for'].split(',')[0] : req.connection.remoteAddress;
    const ipV4 = clientIp.replace(/^.*:/, '')

    let affiliate='website'
    let campaignName='website'

    let campaignId = 1;
    let assign = [];
    let agent = 'admin'
    try {

        if(token !== config.defaultSignUpToken){

            campaignId = (parseInt(campaign) !== NaN)? parseInt(campaign) : 1
           
            assign = await db.query(`call btcwe.get_assign_aff_to_campaign(${campaignId},'${token}')`)

            if(assign.length > 0){
                affiliate = assign[0].aff_name
                campaignName = assign[0].camp_name
            }else{
                res.json({
                    status: "error",
                    message: 'token or campain id error'
                    });
                    return;
            }
        }

        assignToAgentByCampaign = await db.query(`call btcwe.v2_get_assign_agent_to_campaign(${campaignId})`)

        if(assignToAgentByCampaign.length > 0)
            agent = assignToAgentByCampaign[0].ag_name

        var salt = bcrypt.genSaltSync(saltRounds);
        var hash = bcrypt.hashSync(password, salt);

        const user = await db.query(
            `CALL btcwe.pro_insert_user(
                '${campaignName}',
                '${affiliate}',
                '${email}',
                '${first_name}',
                '${last_name}',
                '${contact}',
                '${hash}',
                '${country_name}',
                '${agent}',
                '${ipV4}'
            )`
        );
        const response = user[0].out1;

    // $2b$10$CBGYoxUI3rRKbXl4eHmQn.VO1g19m8xGa9oBNTgmSgzQi9mj8NL1G
        if (response === 'user already exists') {
            res.json({
                status: "error",
                message: "User already exists."
                });
                return;
        }

        if (response.indexOf('user added successfuly!') === -1) {
            throw response;
        }

        const login = await db.query(
            `CALL btcwe.v2_platform_get_client_data(
                    '${email}'
                )`
        );
        const loginresponse = login[0].object;
        

        let Usertoken = jwt.sign({ email: email },config.secretToken,{expiresIn: "7d"});

        const clientID = parseInt(user[0].out1.substring((user[0].out1.indexOf('user_id: ')+8)))

        if(assign.length > 0){
            const insert_aff_pixel_log = await db.query(`select insert_aff_pixel_log(${campaignId},${clientID},${assign[0].aff_id},'${param1}','${param2}')`)
        }

        if(assignToAgentByCampaign.length > 0){
            await db.query(`call btcwe.update_last_agent_auto_assign(${campaignId}, '${agent}')`)
        }

        res.json({
            status: "success",
            message: "User added successfuly!",
            token:Usertoken,
            userInfo: loginresponse
            });
        return;
    } catch (err) {
        console.log(err,'errore signup');
        
        res.json({
            status: "error",
            message: err
            });
        return;
    }
}

exports.verifyemail = async function(req, res){

    const { token } = req.body,
        clientIp = req.headers['x-forwarded-for'] ? req.headers['x-forwarded-for'].split(',')[0] : req.connection.remoteAddress;

    try {
        const user = await db.query(
            `CALL btcwe.verify_email(
                '${clientIp}',
                '${token}'
            )`
        );
        const response = user[0].out1;
                
        if (response !== 'email verified!') {
            throw response;
        }

        res.json({
            status: "success",
            message: response
            });
        return;
    } catch (err) {
        res.json({
            status: "error",
            message: "Something Went Wrong!"
            });
        return;
    }
}

exports.logout = async function(res,req){
    const { email, password } = req.body,
    clientIp = req.headers['x-forwarded-for'] ? req.headers['x-forwarded-for'].split(',')[0] : req.connection.remoteAddress;
}


exports.crm_login = async function(req, res){

    clientIp = req.headers['x-forwarded-for'] ? req.headers['x-forwarded-for'].split(',')[0] : req.connection.remoteAddress;
    const { USER, PASSWORD } = req.body


    try{
        const agent = await db.query(`call btcwe.v2_get_agent_data('${USER}')`)
        if(agent === ''){
            res.json({
                status: "error",
                message: "user not found"
                });
                return;
        }


        bcrypt.compare(PASSWORD, agent[0].password, function(er, re){
            if(er){
                return res.json({
                    status: "error",
                    message: er
                });
            }
            if(re) {
                const token = jwt.sign(
                    {
                      email: agent[0].email,
                      ag_id: agent[0].ag_id,
                      name: agent[0].ag_name,
                      group_id: agent[0].group_id
                    },
                    config.secretToken,
                    {
                        expiresIn: "1h"
                    })
                return res.json({
                    status: "success",
                    message: "Logged in successfully.",
                    token:token,
                    agent:agent[0]
                });
            }else{
                return res.json({
                    status: "error",
                    message: "Error password."
                });
            }
        })

    }catch(err){
        res.json({
            status: "error",
            message: err
            });
            return;
    }

}

exports.verifyCrm = async function(req, res){
    return res.json({
        message: "Token ok"
    });
}

exports.affiliate_crm_login = async function(req, res){

    const { USER, PASSWORD } = req.body

    try{
        const affiliate = await db.query(`call v2_get_affiliate_data('${USER}')`)
        if(affiliate === ''){
            res.json({
                status: "error",
                message: "user not found"
                });
                return;
        }

        bcrypt.compare(PASSWORD, affiliate[0].password, function(er, re){
            if(er){
                return res.json({
                    status: "error",
                    message: er
                });
            }
            if(re) {
                const token = jwt.sign(
                    {
                      aff_id: affiliate[0].aff_id,
                      aff_name: affiliate[0].aff_name,
                      aff_token: affiliate[0].api_token
                    },
                    config.affSecretToken,
                    {
                        expiresIn: "1h"
                    })
                return res.json({
                    status: "success",
                    message: "Logged in successfully.",
                    token:token,
                    aff_name:affiliate[0].aff_name
                });
            }else{
                return res.json({
                    status: "error",
                    message: "Error password."
                });
            }
        })

    }catch(err){
        res.json({
            status: "error",
            message: err
            });
            return;
    }

}