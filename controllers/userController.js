db = require('../sequelize/connection');
const bcrypt = require('bcrypt');
var path = require("path");
var nodemailer = require('nodemailer');
var contactUsTemplate = require('./../emailtemplates').contactUsTemplate;
var forgotPassword = require('./../emailtemplates').forgotPassword;
var wireTransfer = require('./../emailtemplates').wireTransfer;
const saltRounds = 10;
var jwt = require('jsonwebtoken');
// var EmailTemplate = require('email-templates').EmailTemplate;
var transporter = nodemailer.createTransport({
    service: 'gmail',
    auth: {
      user: 'onclickwebsitetest2016@gmail.com',
      pass: 'Onclick_2016'
    }
  });
  const config = require('../config/config.js');
const util = require('util'), fs = require('fs');
const mkdir = util.promisify(fs.mkdir);
function fileUploadDocTypes(type) {
    switch (type) {
        case 'identification':
            return 'Id';
        case 'proofOfAddress':
            return 'utility bill';
        case 'ccImgBack':
            return 'cc back';
        case 'ccImgFront':
            return 'cc front';
        case 'statement':
            return 'bank statement';
        case 'screenshot':
            return 'crypto screenshot transfer';
        case 'proofSlip':
            return 'wire proof';
        default:
            break;
    }
}
function* chunkArray(array, size = 1) {

    var clone = array.slice(0);

    while (clone.length > 0)
        yield clone.splice(0, size);
};

const buildChunkedArray = (array, size) => {
    const chunkedArray = [];

    for (const c of chunkArray(array, size)) {
        chunkedArray.push(c)
    }

    return chunkedArray;
}
exports.details = async function (req, res) {
    const { email } = req.query;

    try {
        const response = await db.query(
            `CALL btcwe.v2_platform_get_client_data('${email}')`
        );
        let results = response[0].object;
        res.json({
            status: "success",
            message: results
        });
        return;
    } catch (err) {
        res.json({
            status: "error",
            message: error
        });
        return;
    }
}
exports.profile = async function (req, res) {
    const {
        email,
        firstName,
        lastName,
        phoneCode,
        phoneNumber,
        street,
        city,
        state,
        country,
        postalCode
    } = req.body;

    try {
        const update = await db.query(
            `CALL btcwe.pro_insert_update_client_details(
                    '${email}',
                    '${firstName}',
                    '${lastName}',
                    '${phoneCode}',
                    '${phoneNumber}',
                    '${street}',
                    '${city}',
                    '${state}',
                    '${country}',
                    '${postalCode}'
                )`
        );

        const updateResponse = update[0].out1;

        if (updateResponse !== 'updated new client details!') {
            throw updateResponse;
        }

        res.json(updateResponse);
    } catch (err) {
        res.json({
            status: "success",
            message: err
        });
        return;
    }
}

// const express = require('express'),
//     router = express.Router(),
exports.dashboard_logs = async function (req, res) {
    const { email } = req.query;

    try {
        const response = await db.query(`CALL btcwe.get_dashboard_log('${email}')`);

        res.json({ results: buildChunkedArray(response, 10) });
        return;
    } catch (err) {
        res.json({
            status: "error",
            message: error
        });
        return;
    }
}

exports.dashboard_completed = async function (req, res) {
    const { email } = req.query;

    try {
        const response = await db.query(`CALL btcwe.get_dashboard_completed('${email}')`);

        res.json({ results: buildChunkedArray(response, 10) });
    } catch (err) {
        res.json({
            status: "error",
            message: error
        });
        return;
    }
}

exports.dashboard_pending = async function (req, res) {
    const { email } = req.query;

    try {
        const response = await db.query(`CALL btcwe.get_dashboard_pending('${email}')`);

        res.json({ results: buildChunkedArray(response, 10) });
    } catch (err) {
        res.json({
            status: "error",
            message: error
        });
        return;
    }
}

exports.dashboard_failed = async function (req, res) {
    const { email } = req.query;

    try {
        const response = await db.query(`CALL btcwe.get_dashboard_failed('${email}')`);

        res.json({ results: buildChunkedArray(response, 10) });
    } catch (err) {
        res.json({
            status: "error",
            message: error
        });
        return;
    }
}

exports.get_bank_account = async function (req, res) {
    const { email } = req.query;

    try {
        const banks = await db.query(`CALL btcwe.get_user_bank_acc('${email}')`);

        res.json({
            status: "success",
            message: banks
        });
        return;
    } catch (err) {
        res.json({
            status: "error",
            message: error
        });
        return;
    }
}

exports.get_all_balance = async function (req, res) {
    const { email } = req.query;

    try {
        const response = await db.query(`CALL btcwe.gross_values_by_username('${email}')`);
        const balance = response[0];
        res.json({
            status: "success",
            message: balance
        });
        return;
    } catch (err) {
        res.json({
            status: "error",
            message: error
        });
        return;
    }
}

exports.get_balance = async function (req, res) {
    const { email } = req.query;

    try {
        const response = await db.query(`CALL btcwe.balance_by_username('${email}')`);
        const balance = response[0];
        res.json({
            status: "success",
            message: balance
        });
        return;
    } catch (err) {
        res.json({
            status: "error",
            message: error
        });
        return;
    }
}

exports.get_fav_assets = async function (req, res) {
    const { email } = req.query;

    try {
        const response = await db.query(`CALL btcwe.fav_assets_by_username('${email}')`);
        const fav_assets = response;
        res.json({
            status: "success",
            message: fav_assets
        });
        return;
    } catch (err) {
        res.json({
            status: "error",
            message: error
        });
        return;
    }
}

exports.add_fav_assets = async function (req, res) {
    const { email,asset_id } = req.query;

    try {
        const response = await db.query(`CALL btcwe.pro_cfd_choose_fav_asset('${email}','${asset_id}')`);
        const fav_assets = response;
        if (response[0].out1 === '') {
            res.json({
                status: "error",
                message: "Something Went Wrong!"
            });
            return;
        }
        res.json({
            status: "success",
            message: response[0].out1
        });
        return;
    } catch (err) {
        res.json({
            status: "error",
            message: error
        });
        return;
    }
}

exports.open_trades_by_username = async function (req, res) {
    const { email } = req.query;

    try {
        const response = await db.query(`CALL btcwe.open_trades_by_username('${email}')`);
        const fav_assets = response;
        const faverror = response['user name error'];
        if (faverror !== undefined) {
            res.json({
                status: "error",
                message: faverror
            });
            return;
        }
        res.json({
            status: "success",
            message: fav_assets
        });
        return;
    } catch (err) {
        res.json({
            status: "error",
            message: error
        });
        return;
    }
}

exports.close_trades = async function (req, res) {
    const { email } = req.body;

    try {
        const response = await db.query(`CALL btcwe.close_trades_by_username('${email}')`);
        const fav_assets = response[0].res1;
        if (fav_assets == '') {
            res.json({
                status: "error",
                message: "No record found to Close Trades!"
            });
            return;
        }
        res.json({
            status: "success",
            message: fav_assets
        });
        return;
    } catch (err) {
        res.json({
            status: "error",
            message: error
        });
        return;
    }
}
exports.CloseOneTrade = async function (req, res) {
    const { username, trade_id } = req.body;

    try {
        const response = await db.query(`CALL btcwe.pro_cfd_crypto_close_trade('${username}','${trade_id}')`);
        const close_msg = response[0].out1;
        if (close_msg === 'closed that trade!') {
            res.json({
                status: "success",
                message: close_msg
            });
            return;
        }
        if (close_msg === 'trade not found!'){
            res.json({
                // status: "success",
                status: "error",
                message: close_msg
            });
        }
        return;
    } catch (err) {
        res.json({
            status: "error",
            message: error
        });
        return;
    }
}


exports.closed_trades_by_username = async function (req, res) {
    const { email } = req.query;

    try {
        const response = await db.query(`CALL btcwe.closed_trades_by_username('${email}')`);
        const fav_assets = response;
        const faverror = response['user name error'];
        if (faverror !== undefined) {
            res.json({
                status: "error",
                message: faverror
            });
            return;
        }
        res.json({
            status: "success",
            message: fav_assets
        });
        return;
    } catch (err) {
        res.json({
            status: "error",
            message: error
        });
        return;
    }
}

exports.bank_account = async function (req, res) {
    const { email,
        iban,
        bank_name,
        bank_address,
        swift,
        account_number
    } = req.body;

    // const address = `${city} ${state}, ${country} ${postal}`;

    try {
        const addAccount = await db.query(
            `CALL btcwe.pro_add_client_bank_acc(
                '${email}',
                '${bank_name}',
                '${bank_address}',
                '${iban}',
                '${swift}',
                '${account_number}'
            )`
        );

        const accountResponse = addAccount[0].out1;

        if (accountResponse !== 'succesfully added bank acc!') {
            throw accountResponse;
        }
        res.json({
            status: "success",
            message: accountResponse
        });
        return;
    } catch (err) {
        res.json({
            status: "error",
            message: error
        });
        return;
    }
}

exports.upload_file = async function (req, res) {
    // const allowedFiles = ['.png', '.jpeg', '.jpg', '.doc', '.docx', '.pdf', '.tif'];
    const { t, amount ,id} = req.body,
        docType = fileUploadDocTypes(t);
    let filename = req.file.filename;
    let source_path = path.resolve(__dirname, `../upload/tmp/`);
    let destination_path = path.resolve(__dirname, `../upload/docs/${id}/${t}/`);
    let folderPath = `/docs/${id}/${t}`;
    fs.mkdir(destination_path, { recursive: true }, (err) => {
        if (err) throw err;
        fs.rename(path.resolve(source_path, filename), path.resolve(destination_path, filename), function (err) {
            if (err) {
                return console.error(err);
            }
        });
    });
   
    try { 
        transporter.verify(function(error, success) {
            if (error) {
              console.log(error);
            } else {
              console.log("Server is ready to take our messages");
            }
          });
    
        let mailOptions = {
            from: "contact@btcwe.io",
            to: id,
            subject: "Btcwe Account Deposit Amount ",
            html: wireTransfer({
                email: id,
                amount: amount
            })
        }
    
        transporter.sendMail(mailOptions, function (error, info) {
            if (error) {
                console.log(error)
            } else {
                res.status(200).json({
                    "messageSent:": info.messageId,
                    "Preview URL:": nodemailer.getTestMessageUrl(info)
                });
            }
        });
        res.json({
            status: "success",
            message: "Deposit request has been sent successfully."
        });
        return;
    } catch (err) {
        console.log('Upload Error :: ', err);

        res.json({
            status: "error",
            message: err
        });
        return;
    }
}

exports.buy_initiate_trade = async function (req, res){
    const { username, amount, value } = req.body;

    try {
        const initiateTrade = await db.query(
            `CALL btcwe.pro_wl_initiate_buy_trade(
                '${username}',
                '${amount}',
                '${value}'
            )`
        );

        const message = initiateTrade[0].out1,
            ref = message.slice(22);

        if (message === 'Client not Found!') {
            res.status(600).json({ message });
        }

        if (message === 'amount too low!') {
            res.status(601).json({ message });
        }

        if (message === 'please refresh rate for user!') {
            res.status(602).json({ message });
        }

        res.json({ message, ref });
    } catch (err) {
        res.status(422).json(err);
    }
}

exports.forgot_password = async function(req,res){ 
    try {
        const client_details = await db.query(
            `CALL btcwe.v2_platform_get_client_data(
                    '${req.body.email}'
                )`
        );
        const response = client_details[0].object;
        const old_pass = response.password;
        const name = response.full_name;
        const token_data = {'pass':old_pass,'email':req.body.email, 'name': name}

        var token = jwt.sign({
            exp: Math.floor(Date.now() / 1000) + (60 * 60),
            data: token_data
          },config.secretToken);
    transporter.verify(function(error, success) {
        if (error) {
          console.log(error);
        } else {
          console.log("Server is ready to take our messages");
        }
      });

    let mailOptions = {
        from: "contact@btcwe.io",
        to: req.body.email,
        subject: "Btcwe Account Reset Link ",
        html: forgotPassword({
            email: req.body.email,
            name: name.split(' ').slice(0, -1).join(' '),
            token: 'http://btcwe.io/reset-password?token='+token
        })
    }

    transporter.sendMail(mailOptions, function (error, info) {
        if (error) {
            console.log(error)
        } else {
            res.status(200).json({
                "messageSent:": info.messageId,
                "Preview URL:": nodemailer.getTestMessageUrl(info)
            });
        }
    });}catch (err) {
        res.json({
            status: "error",
            message: error
        });
        return;
    }
}

exports.contactus = async function(req,res){
    transporter.verify(function(error, success) {
        if (error) {
          console.log(error);
        } else {
          console.log("Server is ready to take our messages");
        }
      });

    let mailOptions = {
        from: "contact@btcwe.io",
        to: "onclickwebsitetest2016@gmail.com",
        subject: "Btcwe Contact Us - " + req.body.subject,
        html: contactUsTemplate({
            email: req.body.email,
            name: req.body.name,
            message: req.body.message
        })
    }

    transporter.sendMail(mailOptions, function (error, info) {
        if (error) {
            console.log(error)
        } else {
            res.status(200).json({
                "messageSent:": info.messageId,
                "Preview URL:": nodemailer.getTestMessageUrl(info)
            });
        }
    });

}

exports.account_snapshot = async function (req, res) {
    const { email } = req.query;

    try {
        const response = await db.query(`CALL btcwe.get_account_snapshot('${email}')`);
        const snap = response;
        res.json({
            status: "success",
            message: snap
        });
        return;
    } catch (err) {
        res.json({
            status: "error",
            message: error
        });
        return;
    }
}


exports.get_user_spread = async function (req, res) {
    const { email } = req.query;

    try {
        const response = await db.query(`call btcwe.spread_by_username('${email}')`);
        res.json({
            status: "success",
            message: response
        });
        return;
    } catch (err) {
        res.json({
            status: "error",
            message: error
        });
        return;
    }
}


exports.create_market_order = async function (req, res) {
    
    const { email, asset_id, trade_type, leverage, amount, rate, stop_loss,profit} = req.body;
    // const stop_loss='',profit='';
    try {
        const response = await db.query(`CALL btcwe.pro_cfd_crypto_make_trade_base_btc('${email}','${asset_id}','${trade_type}','${leverage}','${amount}','${rate}','${stop_loss}','${profit}')`);
        const order = response[0].out1;
        if(order === 'amount parameter error!'){
            res.json({
                status: "error",
                message: "Amount parameter error!"
            });
            return;
        }
        res.json({
            status: "success",
            message: order
        });
        return;
    } catch (err) {
        res.json({
            status: "error",
            message: error
        });
        return;
    }
}

exports.compliance = async function (req, res) {
    const {
        email,
        firstName,
        lastName,
        phoneCode,
        mobile,
        address,
        city,
        state,
        country,
        postal,
    } = req.body;

    try {
        const update = await db.query(
            `CALL btcwe.pro_insert_update_client_details(
                '${email}',
                '${firstName}',
                '${lastName}',
                '${phoneCode}',
                '${mobile}',
                '${address}',
                '${city}',
                '${state}',
                '${country}',
                '${postal}'
                )`
        );
                
        const updateResponse = update[0].out1;

        if (updateResponse !== 'updated new client details!') {
            throw updateResponse;
        }

        res.json(updateResponse);
    } catch (err) {
        console.log(err,'erroreeeeeee')
        res.json({
            status: "error",
            message: err
        });
        return;
    }
}

exports.compliance_upload_file = async function (req, res) {
    const {id, type} = req.body,
    
    docType = fileUploadDocTypes(type);
    let filename = req.file.filename;
    let source_path = path.resolve(__dirname, `../upload/tmp/`);
    let destination_path = path.resolve(__dirname, `../upload/compliance/${id}/`);
    fs.mkdir(destination_path, { recursive: true }, (err) => {
        if (err) throw err;
        fs.rename(path.resolve(source_path, filename), path.resolve(destination_path, filename), function (err) {
            if (err) {
                return console.error(err);
            }
        });
    });
    try{
        let email = id;
        await db.query(`CALL btcwe.pro_insert_cl_file_path('${email}','${type}','0','${filename}')`);
        // const order = response[0].out1;
                res.json({
                    status: "success",
                    message: "uploaded!",
                    // uploadedImageUrl: destination_path+'/'+type + '_' +filename
                });
                return;
            } catch (err) {
                console.log('Upload Error :: ', err);
        
                res.json({
                    status: "error",
                    message: err
                });
                return;
            }
}


exports.complianceImage = async function (req, res) {
    const { email } = req.query;

    try {
        let type = ['id','id back', 'utility bill', 'cc front', 'cc back'];
        let results=[];
        for(i = 0; i<type.length; i++){
            const response = await db.query(
                `CALL btcwe.crm_pro_get_doc_file_path('${email}','${type[i]}','0')`
            );
            results.push({type:type[i] , item: response[0].out1});
    }
            res.json({
                status: "success",
                message: results
            }); 
            return;
        
    } catch (err) {
        res.json({
            status: "error",
            message: error
        });
        return;
    }
}


exports.make_withdrawal_request = async function(req, res){
    const {email,method,amount,comments} = req.body

    try {
        const response = await db.query(`CALL btcwe.pro_insert_wd_request('${email}','${amount}')`);
        const with_req = response[0].out1;
                if (with_req === 'inserted wd request!') {
            res.json({
                status: "success",
                message: 'Approved WD request'
            });
            return;
        }
        if (with_req === 'no balance for client!') {
            res.json({
                status: "error",
                message: "No balance"
            });
            return;
        }
        if (with_req === 'client already has pending WD request!') {
            res.json({
                status: "error",
                message: "Your Withdraw Request Already Has Been Pending"
            });
            return;
        }
        res.json({
            status: "error",
            message: with_req
        });
        return;

    } catch (err) {
        res.json({
            status: "error",
            message: error
        });
        return;
    }
}

// exports.upload_profile_photo = async function (req, res) {
//     const {id, type} = req.body,
//     docType = fileUploadDocTypes(type);
//     let filename = req.file.filename;
//     let source_path = path.resolve(__dirname, `../upload/tmp/`);
//     let destination_path = path.resolve(__dirname, `../upload/profile/${id}/`);
//     fs.mkdir(destination_path, { recursive: true }, (err) => {
//         if (err) throw err;
//         fs.rename(path.resolve(source_path, filename), path.resolve(destination_path, type + '_' +filename), function (err) {
//             if (err) {
//                 return console.error(err);
//             }
//         });
//     });
//     try{
//         res.json({
//             status: "success",
//             message: "uploaded!",
//             uploadedImageUrl: type + '_' +filename
//         });
//         return;
//     } catch (err) {
//         console.log('Upload Error :: ', err);

//         res.json({
//             status: "error",
//             message: err
//         });
//         return;
//     }
//     }

