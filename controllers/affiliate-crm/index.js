const db = require('../../sequelize/connection');



exports.update_callback_url = async function(req,res){
    const {campaignID,callbackUrl} = req.body
    try{
        const response = await db.query(`call allure.update_callback_url(${campaignID}, ${req.aff_id}, '${callbackUrl}')`)
        if(response[0].var1 != 'callback_url updated')
            throw response[0].var1;
        res.status(200); 
        res.json({
            message: response[0].var1
            });
        return
    }catch(err){
        res.json({
            status: "error",
            message: err
            });
            return;
    }
}

exports.get_affiliate_leads = async function(req, res){

    try {
        const campaignsLeads = await db.query(`call v2_get_aff_leads(${req.aff_id})`)
        res.json(campaignsLeads);
        return;
    } catch (err) {
        res.json({
            status: "error",
            message: err
            });
        return;
    }
}

exports.get_campaigns = async function(req, res){

    try {
        const campaigns = await db.query(`call v2_get_aff_campaigns(${req.aff_id})`)
        res.json(campaigns);
        return;
    } catch (err) {
        res.json({
            status: "error",
            message: err
            });
        return;
    }
}


exports.add_new_campaign = async function(req, res){
    
    const {CAMPAIGN_NAME,CAMPAIGN_DESCRIPTION}= req.body
    try{
        let message = await db.query(`call allure.pro_insert_campaign('${CAMPAIGN_NAME}','${CAMPAIGN_DESCRIPTION}')`)
        if(message[0].out1 != 'inserted campaign!'){
            res.json({message:message[0].out1});
            return;
        }
        message = await db.query(`call allure.pro_assign_aff_to_campaign('${req.aff_name}','${CAMPAIGN_NAME}')`)
        res.json({message: message[0].out1});
        return;

    }catch(err){
        res.json({
            status: "error",
            message: err
            });
            return;
    }
}



