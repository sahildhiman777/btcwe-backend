const db = require('../sequelize/connection');
const bcrypt = require('bcrypt');
const saltRounds = 10;
var jwt = require('jsonwebtoken');
const config = require('../config/config.js');
const assets = require('../real-time-rates');
const path = require('path');

exports.get_btc_eur_rate = async function (req, res) {
    try {
        const getRate = await db.query(`CALL btcwe.get_btc_eur_rate()`),
            rate = getRate[0].object.rate;

            res.json({
                rate: rate
                });
                return;
    } catch (err) {
        res.status(422).json(err);
        return;
    }
}

exports.get_currency_rate = async function(req, res){
    const { email } = req.query;

    try {
        const getRate = await db.query(`CALL btcwe.get_trade_rate('${email}', 'buy')`),
            rate = getRate[0].object.rate;

        res.json({ rate });
        return;
    } catch (err) {
        res.json({
            status: "error",
            message: err
            });
        return;
    }
}

exports.get_assets = async function(req, res){ 
    
    const {email, assets_type} = req.query;
    
    //console.log(assets.assetsList[0],'assets.assetsList')
    try {
        // const getRate = await db.query('SELECT `assets_crypto`.`id`,`assets_crypto`.`asset`,`assets_crypto`.`market_rate`,`assets_crypto`.`buy_rate`,`assets_crypto`.`sell_rate`,`assets_crypto`.`spread`,`assets_crypto`.`max`,`assets_crypto`.`min`, `assets_crypto`.`base_info`, `assets_crypto`.`quote_info`, `assets_crypto`.`base_icon`, `assets_crypto`.`quote_icon` FROM `btcwe`.`assets_crypto`'),

        // const getRate = await db.query('SELECT * FROM `btcwe`.`assets_crypto`'),
        //const getRate = await db.query(`CALL btcwe.trade_rates_by_username_and_type('${email}', '${assets_type}')`)
        //console.log(getRate[0],'getRate')
        rate = assets.assetsList//getRate;
        res.json({ rate });
        return;
    } catch (err) {
        res.json({
            status: "error",
            message: err
            });
        return;
    }
}

exports.get_assets_history = async function(req, res){ 
    
    try {
        const getData = await db.query('call btcwe.get_all_assets_history_data(1,15,3);')
        
        res.json(getData);
        return;
    } catch (err) {
        res.json({
            status: "error",
            message: err
            });
        return;
    }
}

exports.get_fiatassets = async function(req, res){
    try {
        // const getRate = await db.query('SELECT `assets_fiat`.`id`,`assets_fiat`.`asset`,`assets_fiat`.`market_rate`,`assets_fiat`.`buy_rate`,`assets_fiat`.`sell_rate`,   `assets_fiat`.`spread`,`assets_fiat`.`max`,`assets_fiat`.`min`, `assets_fiat`.`base_info`, `assets_fiat`.`quote_info`, `assets_fiat`.`base_icon`, `assets_fiat`.`quote_icon` FROM `btcwe`.`assets_fiat`'),
        const getRate = await db.query('SELECT * FROM `btcwe`.`assets_fiat`'),
            rate = getRate[0];
        res.json({ rate });
        return;
    } catch (err) {
        res.json({
            status: "error",
            message: err
            });
        return;
    }
}

exports.get_comassets = async function(req, res){
    try {
        // const getRate = await db.query('SELECT `assets_commodities`.`id`,`assets_commodities`.`asset`,`assets_commodities`.`market_rate`,`assets_commodities`.`buy_rate`,`assets_commodities`.`sell_rate`,`assets_commodities`.`spread`,`assets_commodities`.`max`,`assets_commodities`.`min`, `assets_commodities`.`base_info`, `assets_commodities`.`quote_info`, `assets_commodities`.`base_icon`, `assets_commodities`.`quote_icon` FROM `btcwe`.`assets_commodities`'),
        const getRate = await db.query('SELECT * FROM `btcwe`.`assets_commodities`'),
            rate = getRate[0];
        res.json({ rate });
        return;
    } catch (err) {
        res.json({
            status: "error",
            message: err
            });
        return;
    }
}

exports.get_etfassets = async function(req, res){
    try {
        // const getRate = await db.query('SELECT `assets_etf`.`id`,`assets_etf`.`asset`,`assets_etf`.`market_rate`,`assets_etf`.`buy_rate`,`assets_etf`.`sell_rate`,`assets_etf`.`spread`, `assets_etf`.`base_info`, `assets_etf`.`quote_info`, `assets_etf`.`base_icon`, `assets_etf`.`quote_icon`, `assets_etf`.`max`, `assets_etf`.`min` FROM `btcwe`.`assets_etf`'),

        const getRate = await db.query('SELECT * FROM `btcwe`.`assets_etf`'),
            rate = getRate[0];
        res.json({ rate });
        return;
    } catch (err) {
        res.json({
            status: "error",
            message: err
            });
        return;
    }
}

exports.get_all_fav_assets = async function (req, res) {
    // const { email } = req.query;

    try {
        // const responseEtf = await db.query('SELECT `assets_etf`.`id`,`assets_etf`.`asset`,`assets_etf`.`market_rate`,`assets_etf`.`buy_rate`,`assets_etf`.`sell_rate`,`assets_etf`.`spread`, `assets_etf`.`base_info`, `assets_etf`.`quote_info`, `assets_etf`.`base_icon`, `assets_etf`.`quote_icon` FROM `btcwe`.`assets_etf`'),
        const responseEtf = await db.query('SELECT * FROM `btcwe`.`assets_etf`'),
        etf = responseEtf[0];
        // const responseCom = await db.query('SELECT `assets_commodities`.`id`,`assets_commodities`.`asset`,`assets_commodities`.`market_rate`,`assets_commodities`.`buy_rate`,`assets_commodities`.`sell_rate`,`assets_commodities`.`spread`,`assets_commodities`.`max`,`assets_commodities`.`min`, `assets_commodities`.`base_info`, `assets_commodities`.`quote_info`, `assets_commodities`.`base_icon`, `assets_commodities`.`quote_icon` FROM `btcwe`.`assets_commodities`'),
        const responseCom = await db.query('SELECT * FROM `btcwe`.`assets_commodities`'),
        com = responseCom[0];
        // const responseFiat = await db.query('SELECT `assets_fiat`.`id`,`assets_fiat`.`asset`,`assets_fiat`.`market_rate`,`assets_fiat`.`buy_rate`,`assets_fiat`.`sell_rate`,   `assets_fiat`.`spread`,`assets_fiat`.`max`,`assets_fiat`.`min`, `assets_fiat`.`base_info`, `assets_fiat`.`quote_info`, `assets_fiat`.`base_icon`, `assets_fiat`.`quote_icon` FROM `btcwe`.`assets_fiat`'),
        const responseFiat = await db.query('SELECT * FROM `btcwe`.`assets_fiat`'),
        fiat = responseFiat[0];
        // const responseCrypt = await db.query('SELECT `assets_crypto`.`id`,`assets_crypto`.`asset`,`assets_crypto`.`market_rate`,`assets_crypto`.`buy_rate`,`assets_crypto`.`sell_rate`,`assets_crypto`.`spread`,`assets_crypto`.`max`,`assets_crypto`.`min`, `assets_crypto`.`base_info`, `assets_crypto`.`quote_info`, `assets_crypto`.`base_icon`, `assets_crypto`.`quote_icon` FROM `btcwe`.`assets_crypto`'),
        const responseCrypt = await db.query('SELECT * FROM `btcwe`.`assets_crypto`'),
        crypt = responseCrypt[0];
        let rate= crypt.concat(com, etf,fiat);
        res.json({
            status: "success",
            message: rate
        });
        return;
    } catch (err) {
        res.json({
            status: "error",
            message: error
        });
        return;
    }
}

exports.btcValueForRemoteWidget = async function(req, res){
    try {
        const response = await db.query('call `btcwe`.`get_btc_address`()'),
            rate = response[0].object;
        res.json({
            status: "success",
            message: rate
        });
        return;
    } catch (err) {
        res.json({
            status: "error",
            message: err
            });
        return;
    }
}

exports.getWireDetails = async function(req, res){
    try {
        const response = await db.query('call `btcwe`.`get_wire_details`()'),
            details = response[0].object;
        res.json({
            status: "success",
            message: details
        });
        return;
    } catch (err) {
        res.json({
            status: "error",
            message: err
            });
        return;
    }
}

exports.verifyToken = async function(req, res){
    const {token} = req.query;
    try {
        const details=jwt.verify(token, config.secretToken, function(err, decoded) {
            if (err) {
                return res.json({ success: false, message: err.message });
            } else {
                // if everything is good, save to request for use in other routes
                req.decoded = decoded;
                return req.decoded;
            }
            
          });
        res.json({
            status: "success",
            message: details
        });
        return;
    } catch (err) {
        res.json({
            status: "error",
            message: err
            });
        return;
    }
}

exports.updateNewPassword = async function(req, res){
    const {email,password,old_password} = req.body;
    try {
        const login = await db.query(
            `CALL btcwe.v2_platform_get_client_data(
                    '${email}'
                )`
        );
        const responselogin = login[0].object;
        const real_password = responselogin.password;
        if(!bcrypt.compareSync(old_password, real_password)){
            res.json({
                status: "error",
                message: "Old Password Not Matched !!"
            });
            return;
        }
        
        var salt = bcrypt.genSaltSync(saltRounds);
        var hash = bcrypt.hashSync(password, salt);
        const response = await db.query(`call btcwe.pro_update_user_password('${email}','${real_password}','${hash}');`);
        const update = response[0].out1;
        res.json({
            status: "success",
            message: update
        });
        return;
    } catch (err) {
        res.json({
            status: "error",
            message: err
            });
        return;
    }
}

exports.graphData = async function(req, res){
    const {asset_id, interval, days} = req.query;
    try {        
        const response = await db.query(`call btcwe.get_graph_data('${asset_id}','${interval}','${days}');`);
        
        res.json({
            status: "success",
            message: response
        });
        return;
    } catch (err) {
        res.json({
            status: "error",
            message: err
            });
        return;
    }
}

exports.get_buy_sell = async function(req, res){
    
    const {email, id} = req.query;
    try {        
        const response = await db.query(`call btcwe.trade_rate_by_username('${email}','${id}');`);
        
        
        res.json({
            status: "success",
            message: response[0]
        });
        return;
    } catch (err) {
        res.json({
            status: "error",
            message: err
            });
        return;
    }
}

exports.changeNewPassword = async function(req, res){
    const {email,password,old_password} = req.body;
    try {
        var salt = bcrypt.genSaltSync(saltRounds);
        var hash = bcrypt.hashSync(password, salt);
        const response = await db.query(`call btcwe.pro_update_user_password('${email}','${old_password}','${hash}');`);
        const update = response[0].out1;
        res.json({
            status: "success",
            message: update
        });
        return;
    } catch (err) {
        res.json({
            status: "error",
            message: err
            });
        return;
    }
}

exports.tokenverify = async function (req, res) {
    
    const {token} = req.body;
    try {
        const verify=jwt.verify(token, config.secretToken);

        const login = await db.query(
            `CALL btcwe.v2_platform_get_client_data(
                    '${verify.email}'
                )`
        );

        const response = login[0].object;
        var jwttoken = jwt.sign({ email: verify.email }, config.secretToken);
        if (response === '') {
            res.json({
                status: "error",
                message: "Client not found!",
            });
            return;
        }else{
            return res.json({
                status: "success",
                message: "Logged in successfully.",
                token:jwttoken,
                userInfo:response
            });

        }
        
    } catch (err) {
        console.log(err,'error');
        
        res.json({
            status: "error",
            message: "Invalid credentials please verify before login."
            });
        return;
    }
}

exports.test_aff_pixel = async function(req, res){
    
    const {param1,param2}= req.query

        res.status(200).json({
            params: param1 + ' ' + param2,
            message: 'pixel send successful'
            });
            return;
}

exports.get_user_doc = async function(req, res){
    
    try{
    const {email,fileName} = req.params
      const file_path = path.resolve(__dirname,`../upload/compliance/${email}/${fileName}`)
      res.sendFile(file_path);

    } catch (err) {
        console.log(err,'error');
        
        res.json({
            status: "error",
            message: "error"
            });
        return;
    }
  }


  exports.get_country = async function(req,res) {

    clientIp = req.headers['x-forwarded-for'] ? req.headers['x-forwarded-for'].split(',')[0] : req.connection.remoteAddress;
    const ipV4 = clientIp.replace(/^.*:/, '')

    try{
        const response = await db.query(`call btcwe.pro_get_country_by_ip('${ipV4}')`);

        res.json({
            status: "success",
            message: response
        });
        return;
    } catch (err) {
        res.json({
            status: "error",
            message: err
            });
        return;
    }
    
    
  }