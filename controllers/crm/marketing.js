const db = require('../../sequelize/connection');

const bcrypt = require('bcrypt');
const saltRounds = 10;

exports.update_callback_url = async function(req,res){
    const {campaignID,affiliateID,callbackUrl} = req.body
    try{
        const response = await db.query(`call btcwe.update_callback_url(${campaignID}, ${affiliateID}, '${callbackUrl}')`)
        if(response[0].var1 != 'callback_url updated')
            throw response[0].var1;
        res.status(200);    
        return
    }catch(err){
        res.json({
            status: "error",
            message: err
            });
            return;
    }
}

exports.add_new_affiliate = async function(req, res){
    
    const {PASSWORD,USER}= req.body
    //check if exicte

    let salt = bcrypt.genSaltSync(saltRounds);
    let hash = bcrypt.hashSync(PASSWORD, salt);


    try{
        const message = await db.query(`call pro_insert_crm_affiliate('${USER}','${hash}')`)
        if(message[0].out1 === 'affiliate added succesfully!'){
            res.json({
                status: "success",
                message: message[0].out1
                });
                return;
            }
            res.json({
                status: "error",
                message: message[0].out1
                });
                return;
    }catch(err){
        res.json({
            status: "error",
            message: err
            });
            return;
    }
}

exports.change_password = async function(req, res){
    
    const {affiliate,password}= req.body

    let salt = bcrypt.genSaltSync(saltRounds);
    let hash = bcrypt.hashSync(password, salt);


    try{
        const message = await db.query(`call pro_affiliate_change_password('${hash}','${affiliate}')`)
        if(message[0].out1 === 'password changed!'){
        res.json({
            status: "success",
            message: message[0].out1
            });
            return;
        }
        res.json({
            status: "error",
            message: message[0].out1
            });
            return;
    }catch(err){
        res.json({
            status: "error",
            message: err
            });
            return;
    }
}

exports.get_affiliates = async function(req, res){
    const {sec} = req.body
    try {

        const affiliates = await db.query(`call btcwe.v2_get_all_affiliates(${sec})`)
        res.json(affiliates);
        return;
    } catch (err) {
        res.json({
            status: "error",
            message: err
            });
        return;
    }
}

exports.get_All_campaigns_leads = async function(req, res){
    const {sec} = req.body

    try {

        const campaignsLeads = await db.query(`call btcwe.v2_get_All_campaigns_leads(${sec})`)
        res.json(campaignsLeads);
        return;
    } catch (err) {
        res.json({
            status: "error",
            message: err
            });
        return;
    }
}

exports.get_campaigns = async function(req, res){
    const {sec} = req.body
    try {
        const campaigns = await db.query(`call btcwe.v2_get_all_campaigns(${sec})`)
        res.json(campaigns);
        return;
    } catch (err) {
        res.json({
            status: "error",
            message: err
            });
        return;
    }
}


exports.add_new_campaign = async function(req, res){
    
    const {CAMPAIGN_NAME,AFFILIATE_NAME,CAMPAIGN_DESCRIPTION}= req.body
    try{
        let message = await db.query(`call btcwe.pro_insert_campaign('${CAMPAIGN_NAME}','${CAMPAIGN_DESCRIPTION}')`)
        if(message[0].out1 != 'inserted campaign!'){
            res.json({message:message[0].out1});
            return;
        }
        message = await db.query(`call btcwe.pro_assign_aff_to_campaign('${AFFILIATE_NAME}','${CAMPAIGN_NAME}')`)
        res.json({message: message[0].out1});
        return;

    }catch(err){
        res.json({
            status: "error",
            message: err
            });
            return;
    }
}



