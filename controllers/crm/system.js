
const db = require('../../sequelize/connection');

exports.add_new_auto_assign_by_campain = async function(req,res){
    const {CAMPAIGN,AGENT} = req.body;
    try{
        const response = await db.query(`call btcwe.pro_insert_auto_assign_by_camp(${AGENT}, ${CAMPAIGN})`)
        if (response[0].out1 !== 'auto assign agent successfuly'){
            res.json({
                status: "error",
                message: response[0].out1
                });
                return;
        }
        
        res.status(200).json(response);
        return;
    }catch(err){
        res.json({
            status: "error",
            message: err
            });
            return;
    }
    
}

exports.get_all_auto_assign_by_campain = async function(req,res){
    try{
        const response = await db.query("call btcwe.v2_get_all_assign_agents_to_campaigns()")
        res.json(response);
        return;
    }catch(err){
        res.json({
            status: "error",
            message: err
            });
            return;
    }
}

exports.remove_auto_assign_by_campain = async function(req,res){
    const {CAMPAIGN,AGENT} = req.body;
    try{
        const response = await db.query(`call btcwe.pro_remove_auto_assign_by_camp(${AGENT}, ${CAMPAIGN})`)
        if (response[0].out1 !== 'remove agent successfuly'){
            res.json({
                status: "error",
                message: response[0].out1
                });
                return;
        }
        
        res.status(200).json(response);
        return;
    }catch(err){
        res.json({
            status: "error",
            message: err
            });
            return;
    }
}

exports.get_permissions = async function(req,res){

    try{
        const response = await db.query("call btcwe.get_agents_permissions()")
        if(req.isAdminOrManager){
            res.json(response);
            return;
        }

        const permissions = response.filter(per=> per.ag_id == req.agent)
        res.json(permissions);
        return;
    }catch(err){
        res.json({
            status: "error",
            message: err
            });
            return;
    }
}


exports.add_permission = async function(req,res){
    const {PERMISSION,AGENT} = req.body;
    try{
        const response = await db.query(`call btcwe.pro_assign_permission_to_agent(${AGENT}, ${PERMISSION});`)
        if (response[0].out1 !== 'assign permission'){
            res.json({
                status: "error",
                message: response[0].out1
                });
                return;
        }
        
        res.status(200).json(response);
        return;

    }catch(err){
        res.json({
            status: "error",
            message: err
            });
            return;
    }
}

exports.remove_permission = async function(req,res){
    const {PERMISSION,AGENT} = req.body;
    try{
        const response = await db.query(`call btcwe.pro_remove_assign_permission_to_agent(${AGENT}, ${PERMISSION});`)
        if (response[0].out1 !== 'assign removed'){
            res.json({
                status: "error",
                message: response[0].out1
                });
                return;
        }
        
        res.status(200).json(response);
        return;
    }catch(err){
        res.json({
            status: "error",
            message: err
            });
            return;
    }
}