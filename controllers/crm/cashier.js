
const db = require('../../sequelize/connection');
const bcrypt = require('bcrypt');
const saltRounds = 10;
const fs = require('fs')
const path = require('path');
const axios = require("axios");

exports.make_deposit = async function(req, res){
    const {user,deposit} = req.body

    let salt = bcrypt.genSaltSync(10);
    let d = new Date();
    let ref = bcrypt.hashSync(d.toLocaleDateString(), salt);

    if (ref.length > 50)
        ref = ref.substring(ref.length -50, ref.length -1);
    
    try{
        const depResponse = await db.query(`call btcwe.pro_transaction_attempts ('BTCWE Wallet','${user.email}','${deposit.METHOD}','${ref}','${deposit.AMOUNT}','EUR','eur','approved','','${deposit.COMMENTS}')`)
        if(depResponse[0].out1 != 'this tran has been logged and taken care of!' && depResponse[0].out1 != 'transaction has been recorded on this table'){
            res.json(
                {
                    status: "error",
                    message: depResponse[0].out1
                    });
            return;
        }
        
    }catch(err){
        res.json({
            status: "error",
            message: err
            });
        return;
    }

    try {
        const response = await db.query(`CALL btcwe.get_pixel_log_by_user(${user.client_id})`)

        if (response.length > 0 && response[0].status_id == 1){
            let url = response[0].callback.replace('{PARAM1}',response[0].par1)
            url = url.replace('{PARAM2}',response[0].par2)

            const result = await axios.get(url);
            if(result.status == 200){
                const updateStatus = await db.query(`CALL btcwe.update_pixel_log_by_user(${user.client_id},${0},'${JSON.stringify(result.data)}')`)
                if(updateStatus.length > 0 && updateStatus[0].var1 == 'pixel log updated'){
                    res.json({ status:'ok' ,message: 'The deposit and sending pixel was successful' });
                }else{
                    res.json({ status:"error",message: `The deposit and sending pixel was successful but some error on save the data about it`,logs: updateStatus });
                }
            }else{
                res.json({ status: "error",message: 'The deposit was successful but was error on sending pixel' });
            }
        }else{

            res.json({ status:'ok' , message: 'The deposit was successful' });
            // make only deposits : the user is not FTD
        }
        
        return;
    } catch (err) {
        res.json({
            status: "error",
            message: err
            });
        return;
    }
}

exports.make_withdrawal = async function(req, res){
    const {user,withdrawal} = req.body

    let salt = bcrypt.genSaltSync(10);
    let d = new Date();
    let ref = bcrypt.hashSync(d.toLocaleDateString(), salt);
    if (ref.length > 50)
        ref = ref.substring(ref.length -50, ref.length -1);
    
    try{
        const depResponse = await db.query(`call btcwe.pro_transaction_attempts ('BTCWE Wallet','${user.email}','${withdrawal.METHOD}','${ref}','${withdrawal.AMOUNT*-1}','EUR','eur','approved','','${withdrawal.COMMENTS}')`)
        if(depResponse[0].out1 != 'this tran has been logged and taken care of!' && depResponse[0].out1 != 'transaction has been recorded on this table'){
            res.json(
                {
                    status: "error",
                    message: depResponse[0].out1
                    });
            return;
        }
        res.json({ message: 'withdrawal maked' });
        return;
    }catch(err){
        res.json({
            status: "error",
            message: err
            });
        return;
    }
}

exports.make_bonus = async function(req, res){

    const {user,bonus} = req.body

    try {
        const response = await db.query(`call btcwe.pro_multi_insert_update_demo_holdings('${user}', ${bonus});`)
        if(response[0].out1 !== 'succesful bonus round over'){
            res.json({
                status: "error",
                message: response[0].out1
                });
            return;
        }
        res.json(response);
        return;
    } catch (err) {
        res.json({
            status: "error",
            message: err
            });
        return;
    }
}

exports.get_all_client_demo_holdings = async function(req, res){
    const {sec} = req.body
    try {
        const response = await db.query(`call btcwe.v2_get_clients_domo_holdings(${sec});`)
 
        if(req.isAdminOrManager){
            res.json(response);
            return;
        }

        const bonuses = response.filter(bonus=> bonus.ag_id == req.agent)
        res.json(bonuses);
        return;

    } catch (err) {
        res.json({
            status: "error",
            message: err
            });
        return;
    }
}

exports.get_all_client_finance = async function(req, res){
    const {sec} = req.body
    try {
        const response = await db.query(`call btcwe.v2_get_all_client_finance(${sec})`)

        if(req.isAdminOrManager){
            res.json(response);
            return;
        }

        const client_finance = response.filter(fn=> fn.ag_id == req.agent)
        res.json(client_finance);
        return;
        
    } catch (err) {
        res.json({
            status: "error",
            message: err
            });
        return;
    }
}

exports.get_all_withdrawals_request = async function(req, res){
    try {
        const response = await db.query(`call btcwe.v2_get_all_withdrawals_request()`)

        if(req.isAdminOrManager){
            res.json(response);
            return;
        }

        const withdrawals_req = response.filter(wd=> wd.ag_id == req.agent)
        res.json(withdrawals_req);
        return;

    } catch (err) {
        res.json({
            status: "error",
            message: err
            });
        return;
    }
}


exports.update_withdrawal_request = async function(req, res){

    const {user,update} = req.body

    try {
        const response = await db.query(`call btcwe.pro_update_withdrawal_request('${user}','${update.STATUS}', '${update.METHOD}','${update.COMMENTS}');`)
        if(response[0].out1 !== 'Approved WD request!!' && response[0].out1 !== 'wd status updated!'){
            res.json({
                status: "error",
                message: response[0].out1
                });
            return;
        }
        res.json(response);
        return;
    } catch (err) {
        res.json({
            status: "error",
            message: err
            });
        return;
    }
}