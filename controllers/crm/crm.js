const db = require('../../sequelize/connection');
const bcrypt = require('bcrypt');
const saltRounds = 10;
const fs = require('fs')
const path = require('path');
const axios = require("axios");


exports.get_agents = async function(req, res){
    const {sec} = req.body
    try {
        const users = await db.query(`call btcwe.v2_get_all_agents('${sec}')`)
        res.json({ users });
        return;
    } catch (err) {
        res.json({
            status: "error",
            message: err
            });
        return;
    }
}

exports.add_new_agent = async function(req, res){
    
    const {EMAIL,PASSWORD,USER,GROUP}= req.body
    //check if exicte

    let salt = bcrypt.genSaltSync(saltRounds);
    let hash = bcrypt.hashSync(PASSWORD, salt);


    try{
        const message = await db.query(`call btcwe.pro_insert_agent('${USER}','${hash}','${EMAIL}',${GROUP},0)`)
        if(message[0].out1 === 'agent added successfuly'){
            res.json({
                status: "success",
                message: message[0].out1
                });
                return;
            }
            res.json({
                status: "error",
                message: message[0].out1
                });
                return;
    }catch(err){
        res.json({
            status: "error",
            message: err
            });
            return;
    }
}

exports.assign_agent_to_group = async function(req, res){
    
    const {agent,group}= req.body

    try{
        const message = await db.query(`call btcwe.pro_assign_agent_to_group(${agent},${group})`)
        if(message[0].out1 === 'assign to group'){
        res.json({
            status: "success",
            message: message[0].out1
            });
            return;
        }
        res.json({
            status: "error",
            message: message[0].out1
            });
            return;
    }catch(err){
        res.json({
            status: "error",
            message: err
            });
            return;
    }
}



exports.change_password = async function(req, res){
    
    const {agent,password}= req.body

    let salt = bcrypt.genSaltSync(saltRounds);
    let hash = bcrypt.hashSync(password, salt);


    try{
        const message = await db.query(`call btcwe.pro_agent_change_password('${hash}','${agent.agent_name}')`)
        if(message[0].out1 === 'password changed!'){
        res.json({
            status: "success",
            message: message[0].out1
            });
            return;
        }
        res.json({
            status: "error",
            message: message[0].out1
            });
            return;
    }catch(err){
        res.json({
            status: "error",
            message: err
            });
            return;
    }
}

exports.assign_agent = async function(req,res){
    const {users , agent} = req.body;
    let usersString = '';
    for (let [user, checked] of Object.entries(users)) {
        if(checked && user != 'all')
            usersString += `(${user}),`
    }
    if(usersString === ''){
        res.json({
            status: "error",
            message: 'no users checked'
            });
    }
    
    try{
        const message = await db.query(`call btcwe.pro_multi_assign_agent('${usersString}', '${agent}')`)
        res.json({
            message: message
            });
            return;
    }catch(err){
        res.json({
            status: "error",
            message: err
            });
            return;
    }
    
}

exports.add_new_note = async function(req, res){
    
    const {TITLE,NOTE,STATUS,AGENT,USER}= req.body
    try{
        const message = await db.query(`call btcwe.insert_note('${USER}','${AGENT}','${TITLE}','${NOTE}','${STATUS}')`)
        res.json({
            message: message
            });
            return;
    }catch(err){
        res.json({
            status: "error",
            message: err
            });
            return;
    }
}

exports.get_notes = async function(req, res){
    
    const {sec} = req.body
    try {
        const Allnotes = await db.query(`call btcwe.get_all_notes('${sec}')`)

        if(req.isAdminOrManager){
            res.json(Allnotes);
            return;
        }

        const notes = Allnotes.filter(note=> note.object.agent_id == req.agent)
        res.json(notes);
        return;

    } catch (err) {
        res.json({
            status: "error",
            message: err
            });
        return;
    }
}

