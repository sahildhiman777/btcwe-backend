
const db = require('../../sequelize/connection');
const bcrypt = require('bcrypt');
const saltRounds = 10;
const fs = require('fs')
const path = require('path');
const axios = require("axios");


exports.get_users = async function(req, res){
  const {sec} = req.body
  
  try {
      let users = await db.query(`CALL btcwe.v2_get_all_client_data('${sec}')`)

      if(req.isAdminOrManager){
        res.json({ users });
        return;
    }

    users = users.filter(user=> user.ag_id == req.agent)
    res.json({users});
    return;

  } catch (err) {
      res.json({
          status: "error",
          message: err
          });
      return;
  }
}

exports.update_verification_status = async function(req, res){
    const {user,status} = req.body
    
    try {
        const response = await db.query(`call btcwe.pro_update_verification_status('${user}', '${status}')`)
        if(response[0].out1 !== 'done!'){
            res.json(
                {
                    status: "error",
                    message: response[0].out1
                    });
            return;
        }
        res.json({                     
            status: "success",
            message: response[0].out1 });
        return;
    } catch (err) {
        res.json({
            status: "error",
            message: err
            });
        return;
    }
  }

exports.update_trade_group = async function(req,res){
  const {users , tradeGroup} = req.body;
  let usersString = '';
  for (let [user, checked] of Object.entries(users)) {
      if(checked && user != 'all')
          usersString += `(${user}),`
  }
  if(usersString === ''){
      res.json({
          status: "error",
          message: 'no users checked'
          });
  }

  try{
      const message = await db.query(`call btcwe.pro_multi_update_trade_group('${usersString}', '${tradeGroup}')`)
      res.json({
          message: message
          });
          return;
  }catch(err){
      res.json({
          status: "error",
          message: err
          });
          return;
  }
  
}

exports.update_sale_status = async function(req,res){
  const {users , status} = req.body;
  let usersString = '';
  for (let [user, checked] of Object.entries(users)) {
      if(checked && user != 'all')
          usersString += `${user},`
  }
  if(usersString === ''){
      res.json({
          status: "error",
          message: 'no users checked'
          });
  }

  try{
      const message = await db.query(`call btcwe.pro_multi_update_sale_status('${usersString}','${status}')`)
      res.json({
          message: message
          });
          return;
  }catch(err){
      res.json({
          status: "error",
          message: err
          });
          return;
  }
  
}



exports.get_user_files = async function(req,res){
    const { email } = req.params;

    try {
        const type = ['id','id back', 'utility bill', 'cc front', 'cc back'];
        let results=[];
        for(i = 0; i<type.length; i++){
            
            const response = await db.query(
                `CALL btcwe.crm_pro_get_doc_file_path('${email}','${type[i]}','0')`
            );
            results.push({type:type[i] , item: response[0].out1});
    }
            res.json({
                status: "success",
                message: results
            }); 
            return;
        
    } catch (err) {
        res.json({
            status: "error",
            message: error
        });
        return;
    }

}

exports.add_file = async function(req, res){

  const { userEmail ,docName} = req.body
  const dir = `upload/compliance/${userEmail}`
  if(!fs.existsSync(dir))
      fs.mkdirSync(dir)

  const dest = path.resolve(dir, req.file.filename); 
  const filePath =  req.file.filename

  try {
      fs.renameSync(req.file.path, dest)
      const response = await db.query(`call btcwe.pro_insert_cl_file_path('${userEmail}', '${docName}', '0', '${filePath}')`)
      res.json({response:response,filePath:filePath});
      return;
  } catch (err) {
      res.json({
          status: "error",
          message: err
          });
      return;
  }    
  
}
/*
exports.del_file = async function(req,res){
  const {hash,userID,filePath,imgID} = req.params
  const theHash = parseInt(new Date().getTime()/10000)
  if(theHash != hash || !/^\d+$/.test(imgID)){
      res.json({
          status: "error",
          message: "delete error"
          });
          return;
  }

      const file_path = path.resolve(__dirname,`../files/${userID}/${filePath}`)
      try {
          const del = await db.query(`delete from btcwe.cl_docs where img_id=${imgID}`)
          fs.unlinkSync(file_path)
          res.json({ok:del
              });
              return;
        } catch(err) {
          res.json({
              status: "error",
              message: err
              });
              return;
        }
}

*/