
const db = require('../../sequelize/connection');



exports.get_assets_all = async function(req, res){
    try {
        const response = await db.query('SELECT * FROM btcwe.assets_all')
        res.json(response[0]);
        return;
    } catch (err) {
        res.json({
            status: "error",
            message: err
            });
        return;
    }
}

exports.get_all_trades = async function(req, res){
    const {sec} = req.body
    try {
        const response = await db.query(`call btcwe.get_all_trades(${sec})`)
        if(req.isAdminOrManager){
            res.json(response);
            return;
        }

        const trades = response.filter(trade=> trade.ag_id == req.agent)
        res.json(trades);
        return;
    } catch (err) {
        res.json({
            status: "error",
            message: err
            });
        return;
    }
}
