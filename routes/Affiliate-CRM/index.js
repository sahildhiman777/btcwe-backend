const router = require('express').Router();
const affiliateCrmController = require('../../controllers/affiliate-crm');

router.route('/add_new_campaign').post(affiliateCrmController.add_new_campaign);
router.route('/get_all_campaigns').get(affiliateCrmController.get_campaigns);
router.route('/update_callback_url').put(affiliateCrmController.update_callback_url);
router.route('/get_affiliate_leads').get(affiliateCrmController.get_affiliate_leads);

module.exports = router;