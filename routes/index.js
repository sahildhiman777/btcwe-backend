const router = require('express').Router();
const authenticate = require('./authenticate');
const user = require('./user');
const public = require('./public-routes');
const crm = require('./CRM')
const middleware = require('../middleware/verifyTokenCrm');
const affiliateCRM = require ('./Affiliate-CRM')
const middlewareAff = require('../middleware/affiliateAuth');
const auth = require('../middleware/auth');
const verifycountry = require('../middleware/country')

router.use('/v12', verifycountry,authenticate);
router.use('/v12/user', auth,user);
router.use('/v12/public',verifycountry, public);
router.use('/v12/crm',middleware.verifyToken,crm);
router.use('/v12/affiliate_crm',middlewareAff.verifyToken,affiliateCRM)

module.exports = router;