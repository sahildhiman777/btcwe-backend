const router = require('express').Router();
const systemController = require('../../controllers/crm/system');
const middleware = require('../../middleware/verifyTokenCrm');

router.route('/add_new_auto_assign_by_campain').post(systemController.add_new_auto_assign_by_campain);
router.route('/get_all_auto_assign_by_campain').get(systemController.get_all_auto_assign_by_campain);
router.route('/remove_auto_assign_by_campain').delete(systemController.remove_auto_assign_by_campain);
router.route('/get_permissions').get(systemController.get_permissions);
router.route('/add_permission').post(middleware.isAdmin,systemController.add_permission);
router.route('/remove_permission').delete(middleware.isAdmin,systemController.remove_permission);

module.exports = router;