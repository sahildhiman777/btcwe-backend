const router = require('express').Router();
const crmController = require('../../controllers/crm/crm');
const middleware = require('../../middleware/verifyTokenCrm');

router.route('/get_all_notes').post(crmController.get_notes);
router.route('/add_new_note').post(crmController.add_new_note);
router.route('/add_new_agent').post(crmController.add_new_agent);
router.route('/change_password').post(middleware.isAdmin,crmController.change_password);
router.route('/assign_agent').put(crmController.assign_agent);
router.route('/assign_agent_to_group').put(middleware.isAdmin,crmController.assign_agent_to_group);
router.route('/get_all_agents').post(middleware.isAdmin,crmController.get_agents);

module.exports = router;