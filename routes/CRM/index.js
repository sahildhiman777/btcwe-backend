const router = require('express').Router();

const crm = require('./crm')
const cashier = require('./cashier')
const marketing = require('./marketing')
const trading = require('./trading')
const users = require('./users')
const system = require('./system')

router.use('/', crm);
router.use('/', cashier);
router.use('/', marketing);
router.use('/', trading);
router.use('/', users);
router.use('/', system);

module.exports = router;