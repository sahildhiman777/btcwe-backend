const router = require('express').Router();
const usersController = require('../../controllers/crm/users');
const multer = require('multer');



const storage = multer.diskStorage({
    destination: function(req, file, cb) {
      const dir = __dirname + '../../../files/'
      cb(null,dir);
    },
    filename: function(req, file, cb) {
      const name = new Date().getTime().toString() + file.originalname.substring(file.originalname.indexOf('.'))
      cb(null,name);
    }
  });
  
  const fileFilter = (req, file, cb) => {
    if (file.mimetype === 'image/jpeg' || file.mimetype === 'image/png') {
      cb(null, true);
    } else {
      cb(new Error('not Allowed type'), false);
    }
  };
  
  const upload = multer({
    storage: storage,
    limits: {
      fileSize: 1024 * 1024 * 5
    },
    fileFilter: fileFilter
  });
  
  
  
router.route('/add_file').post(upload.single('file'),usersController.add_file);
router.route('/get_all_users').post(usersController.get_users);
//router.route('/del_file/:hash/:userID/:filePath/:imgID').delete(usersController.del_file);
router.route('/get_user_files/:email').get(usersController.get_user_files);
router.route('/update_sale_status').put(usersController.update_sale_status);
router.route('/update_trade_group').put(usersController.update_trade_group);
router.route('/update_verification_status').put(usersController.update_verification_status);

module.exports = router;