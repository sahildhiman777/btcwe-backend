
const router = require('express').Router();
const tradingController = require('../../controllers/crm/trading');

router.route('/get_assets_all').get(tradingController.get_assets_all);
router.route('/get_all_trades').post(tradingController.get_all_trades);


module.exports = router;