
const router = require('express').Router();
const cashierController = require('../../controllers/crm/cashier');
const middleware = require('../../middleware/verifyTokenCrm');

router.route('/get_all_client_finance').post(cashierController.get_all_client_finance);
router.route('/make_deposit').post(middleware.isAdmin,cashierController.make_deposit);
router.route('/make_withdrawal').post(middleware.isAdmin,cashierController.make_withdrawal);
router.route('/make_bonus').post(cashierController.make_bonus);
router.route('/get_all_client_demo_holdings').post(cashierController.get_all_client_demo_holdings);
router.route('/get_all_withdrawals_request').get(cashierController.get_all_withdrawals_request);
router.route('/update_withdrawal_request').post(middleware.isAdmin,cashierController.update_withdrawal_request);


module.exports = router;