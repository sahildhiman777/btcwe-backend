const router = require('express').Router();
const marketingController = require('../../controllers/crm/marketing');
const middleware = require('../../middleware/verifyTokenCrm');



router.route('/add_new_affiliate').post(middleware.isManagerOrAdmin,marketingController.add_new_affiliate);
router.route('/get_all_affiliates').post(middleware.isManagerOrAdmin,marketingController.get_affiliates);
router.route('/add_new_campaign').post(middleware.isManagerOrAdmin,marketingController.add_new_campaign);
router.route('/get_all_campaigns').post(middleware.isManagerOrAdmin,marketingController.get_campaigns);
router.route('/update_callback_url').put(middleware.isManagerOrAdmin,marketingController.update_callback_url);
router.route('/get_All_campaigns_leads').post(middleware.isManagerOrAdmin,marketingController.get_All_campaigns_leads);
router.route('/affiliate_change_password').post(middleware.isAdmin,marketingController.change_password);

module.exports = router;