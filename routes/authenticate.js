const router = require('express').Router();
const basicController = require('../controllers/basicController');
const middleWare = require('../middleware/verifyTokenCrm');

router.route('/login').post(basicController.authenticate);
router.route('/register').post(basicController.register);
router.route('/verifyemail').post(basicController.verifyemail);
router.route('/crm_login').post(basicController.crm_login);
router.route('/verifyCrm').get([middleWare.verifyToken],basicController.verifyCrm);
router.route('/affiliate_crm_login').post(basicController.affiliate_crm_login);

module.exports = router;
