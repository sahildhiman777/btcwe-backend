const router = require('express').Router();
const auth = require('../middleware/auth');
var path = require("path");
const fs = require('fs');
const userController = require('../controllers/userController');
const multer = require('multer');
var storage = multer.diskStorage(
    {
        destination:function (req, file, cb) {
            cb( null, __dirname +'../../upload/tmp');
        },
        filename: function ( req, file, cb ) {
            const fileName = file.originalname,
            filePath = path.parse(fileName),
            fileExtension = (filePath.ext).toLowerCase(),
            uniqueFileName = `${new Date().getTime()}${fileName}`;

            cb( null, `${uniqueFileName}`);
        }
    }
);

var upload = multer({ storage: storage});
router.route('/profile').post(auth, userController.profile);
router.route('/dashboard_logs').get(auth, userController.dashboard_logs);
router.route('/dashboard_completed').get(auth, userController.dashboard_completed);
router.route('/dashboard_pending').get(auth, userController.dashboard_pending);
router.route('/dashboard_failed').get(auth, userController.dashboard_failed);
router.route('/bank_account').post(auth, userController.bank_account);
router.route('/get_bank_account').get(auth, userController.get_bank_account);
router.route('/details').get(auth, userController.details);
router.route('/upload_file').post(auth, upload.single('file'), userController.upload_file);
router.route('/buy_initiate_trade').post(auth, userController.buy_initiate_trade);
router.route('/forgot_password').post(userController.forgot_password);
router.route('/contact_us').post(auth, userController.contactus);
router.route('/get_balance').get(auth, userController.get_balance);
router.route('/get_all_balance').get(auth, userController.get_all_balance);
router.route('/get_fav_assets').get(auth, userController.get_fav_assets);
router.route('/add_fav_assets').get(auth, userController.add_fav_assets);
router.route('/open_trades_by_username').get(auth, userController.open_trades_by_username);
router.route('/closed_trades_by_username').get(auth, userController.closed_trades_by_username);
router.route('/close_trades').post(auth, userController.close_trades);
router.route('/account_snapshot').get(auth, userController.account_snapshot);
router.route('/create_market_order').post(auth, userController.create_market_order);
router.route('/compliance').post(auth, userController.compliance);
// router.route('/upload_profile_photo').post(upload.single('file'), userController.upload_profile_photo);
router.route('/compliance_upload_file').post(auth, upload.single('file'), userController.compliance_upload_file);
router.route('/compliance_image').get(auth, userController.complianceImage);
router.route('/close_one_trade').post(auth, userController.CloseOneTrade);
router.route('/make_withdrawal_request').post(auth,userController.make_withdrawal_request);
router.route('/get_user_spread').get(auth,userController.get_user_spread);

module.exports = router;