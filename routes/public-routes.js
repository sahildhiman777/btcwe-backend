const router = require('express').Router();
const publicController = require('../controllers/publicController');

router.route('/get-btc-rate').get(publicController.get_btc_eur_rate);
router.route('/get-currency-rate').get(publicController.get_currency_rate);
router.route('/get_assets').get(publicController.get_assets);
router.route('/get_assets_history').get(publicController.get_assets_history);
router.route('/get_etfassets').get(publicController.get_etfassets);
router.route('/get_comassets').get(publicController.get_comassets);
router.route('/get_fiatassets').get(publicController.get_fiatassets);
router.route('/get_all_fav_assets').get(publicController.get_all_fav_assets);
router.route('/get_btc_remote_value').get(publicController.btcValueForRemoteWidget);
router.route('/get_wire_details').get(publicController.getWireDetails);
router.route('/verify_token').get(publicController.verifyToken);
router.route('/update_new_password').post(publicController.updateNewPassword);
router.route('/graph_data').get(publicController.graphData);
router.route('/get_buy_sell').get(publicController.get_buy_sell);
router.route('/changeNewPassword').post(publicController.changeNewPassword);
router.route('/tokenverify').post(publicController.tokenverify);
router.route('/test_aff_pixel').get(publicController.test_aff_pixel); 
router.route('/get_user_doc/:email/:fileName').get(publicController.get_user_doc);
router.route('/get_country').get(publicController.get_country);

module.exports = router;