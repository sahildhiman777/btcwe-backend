const cron = require('node-cron');
const db = require('../sequelize/connection');
// Import coingecko-api
const CoinGecko = require('coingecko-api');
const socket = require('./socket');
const UsdRates = require('./assets/usdRates')
const AssetsList = require('./assets/assetsList')
let realTime;
// Initiate the CoinGecko API Client
const CoinGeckoClient = new CoinGecko();
const usdRates = new UsdRates();
const assets = new AssetsList();

const coinIDs = [
    'bitcoin',
    'ethereum',
    'litecoin',
    'monero',
    'ethereum-classic',
    'zcash',
    'ripple',
    'stellar',
    'bitcoin-cash',
    'binancecoin',
    'tron',
    'cardano',
    'qtum',
    'neo',
    'wax',
    'dogecoin',
    'steem',
    'petrodollar'
];



const start = () => {
    realTime = socket.connection()
    cron.schedule('*/6 * * * * * ', function () {
    cronJob();
});}

cronJob = async function () {
    let res = await CoinGeckoClient.simple.price({
        ids: coinIDs,
        vs_currencies: ['usd','eur','gbp'],
        include_24hr_vol:true,
        include_24hr_change:true,
        include_last_updated_at:true
    });

    

    if(res.success) {
        setUpdates(res.data) 
    }
    else {
        console.log("Error : ", res.message);
    }
}

const setUpdates = data =>{

    let base,quote;
    let roundDigits = 4;
    try{
        assets.getAssetList().forEach(asset=>{
            base = data[asset.base];
            if(asset.type !== 'crypto2fiat')
                roundDigits = 9
            else
                roundDigits = 4
            if(asset.type === 'crypto2fiat' && base.usd !== usdRates.getRateByCoin(asset.base)){
                    asset.setRate(round(data[asset.base][asset.quote],4))
                    asset.setChange24H(round(data[asset.base][`${asset.quote}_24h_change`],2))
                    //console.log(asset,'change')
                    insertToHistory(asset)
            }            
            else if(asset.type !== 'crypto2fiat' && (base.usd !== usdRates.getRateByCoin(asset.base) || data[asset.quote].usd !== usdRates.getRateByCoin(asset.quote))){
                        quote = data[asset.quote];
                        let rate = base.usd/quote.usd;

                        asset.setRate(round(rate,9))

                        let ago24Base = base.usd/(base.usd_24h_change/100 + 1)
                        let ago24Quote = quote.usd/(quote.usd_24h_change/100 + 1)
                        let ago24Rate = ago24Base/ago24Quote;
                        let change24h = (rate-ago24Rate)/ago24Rate*100
                        asset.setChange24H(round(change24h,2))
                        change = true;
                        //console.log(asset,'change')
                        insertToHistory(asset)
                }else if(Math.floor(Math.random() * 10) === 1){
                    asset.setRate(round(asset.market_rate*1.000001,roundDigits))
                    asset.setChange24H(round(asset['24h_change']*1.000001,2))
                }else if (Math.floor(Math.random() * 10) === 2){
                    asset.setRate(round(asset.market_rate*0.999999,roundDigits))
                    asset.setChange24H(round(asset['24h_change']*0.999999,2))
                }
        })
            


    }catch(error){
        console.log(error,'error')
    }
    //console.log(assets.getAssetList(),'assets')
    usdRates.setRates(data)
    
    realTime.sendEvent('rate_update',assets.getAssetList())

}

const insertToHistory = async (asset) =>{
    try{
        await db.query(`call btcwe.insert_rates_to_history(${asset.id}, ${asset.market_rate}, ${asset['24h_change']}, ${asset.min}, ${asset.max});`)
    }catch(error){
        console.log(error,'error')
    }
}

const round = (num,d) =>{
    const digits = Math.pow(10, d)
    return Math.round((num + Number.EPSILON) * digits) / digits
}

module.exports = {
    start:start,
    assetsList:assets.getAssetList()
};

/*
  steem:
   { usd: 0.199526,
     usd_24h_vol: 2871793.950634556,
     usd_24h_change: -5.820396049461218,
     last_updated_at: 1583749217 },
  'ethereum-classic':*/