class Asset{
    constructor(id,type,code,base,quote,base_info,quote_info,base_icon,quote_icon){
        this.id = id;
        this.type = type;
        this.asset = code;
        this.base = base;
        this.quote = quote;
        this.market_rate = 0;
        this.buy_rate = 0;
        this.sell_rate = 0;
        this.max = 0;
        this.min = Number.MAX_SAFE_INTEGER;
        this.base_info = base_info;
        this.quote_info = quote_info;
        this.base_icon = base_icon;
        this.quote_icon = quote_icon;
        this['24h_change'] = 0;
        this.day = new Date()
    }
    setRate(rate){
        this.market_rate = rate
        this.buy_rate = rate;
        this.sell_rate = rate;
        if(this.sameDay(this.day,new Date())){
            if(rate > this.max)
                this.max = rate;
            if(rate < this.min)
                this.min = rate
        }else{
            this.day = new Date()
        }
    }
    setChange24H(val){
        this['24h_change'] = val
    }
    setBuyRate(spread){
        this.buyRate = this.marketRate * (1 - spread)
    }
    setSellRate(spread){
        this.SellRate = this.marketRate * (1 - spread)
    }
    getAsset(){
        return this
    }

    sameDay(d1, d2) {
        return d1.getFullYear() === d2.getFullYear() &&
          d1.getMonth() === d2.getMonth() &&
          d1.getDate() === d2.getDate();
      }
}

module.exports = Asset;