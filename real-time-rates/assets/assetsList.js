const Asset = require('./asset')

class AssetsList{
    constructor(){
        this.assets = assets.map(asset=>{
            return(new Asset(asset.id,asset.type,asset.code,asset.base,asset.quote,asset.base_info,asset.quote_info,asset.base_icon,asset.quote_icon))
        })
        //console.log(assets,assets)
    }
    setAssetRate(id,rate){
        //this.assets[id].setRate(rate)
    }
    getAssetList(){
        return this.assets;
    }
}

const assets = [
    {id:1,type:'crypto2crypto',code:'XRP/BTC',base:'ripple',quote:'bitcoin',base_info:`Ripple is a real-time gross settlement system, currency exchange and remittance network created by Ripple Labs Inc., a US-based technology company.
    Developed by: Ripple Labs
    Written in: C++
    Supply Limit: Still in production 
    Founders: Chris Larsen, Jed McCaleb`,quote_info:`Bitcoin is a decentralized digital currency without a central bank or single administrator that can be sent from user-to-user on the peer-to-peer bitcoin network without the need for intermediaries.
    Initial release: 0.1.0 / 9 January 2009 
    Coins: Unspent outputs of transactions (in multiples of a satoshi)
    Supply limit: 21,000,000
    Founder: Satoshi Takemoto`,base_icon:`ripple.png`,quote_icon:`ripple.png`},
    {id:2,type:'crypto2crypto',code:'BCH/BTC',base:'bitcoin-cash',quote:'bitcoin',base_info:`Bitcoin Cash is a cryptocurrency. In mid-2017, a group of developers wanting to increase bitcoin's block size limit prepared a code change. The change, called a hard fork, took effect on 1 August 2017. As a result, the bitcoin ledger called the blockchain and the cryptocurrency split in two.'ADA', 'cardano'
 
    First block after split (block #478559): 1 August 2017 
    Block #1: 9 January 2009 
    Block reward: 12.5 BCH
    Genesis block: 3 January 2009 
    Coins: Unspent outputs of transactions
    Supply limit: 21,000,000 BCH`,quote_info:`Bitcoin is a decentralized digital currency without a central bank or single administrator that can be sent from user-to-user on the peer-to-peer bitcoin network without the need for intermediaries.
    Initial release: 0.1.0 / 9 January 2009 
    Coins: Unspent outputs of transactions (in multiples of a satoshi)
    Supply limit: 21,000,000
    Founder: Satoshi Takemoto`,base_icon:'bch.png',quote_icon:'btc.png'},
    {id:3,type:'crypto2crypto',code:'XLM/BTC',base:'stellar',quote:'bitcoin',base_info:`Stellar is an open-source, decentralized protocol for digital currency to fiat currency transfers which allows cross-border transactions between any pair of currencies. The Stellar protocol is supported by a nonprofit, the Stellar Development Foundation.
    Original author(s): Jed McCaleb, Joyce Kim
    Developer(s): Stellar Development Foundation
    Initial release date: April 8, 2015
    Written in: C++, Go, JavaScript, Java, Python, Ruby`,quote_info:`Bitcoin is a decentralized digital currency without a central bank or single administrator that can be sent from user-to-user on the peer-to-peer bitcoin network without the need for intermediaries.
    Initial release: 0.1.0 / 9 January 2009 
    Coins: Unspent outputs of transactions (in multiples of a satoshi)
    Supply limit: 21,000,000
    Founder: Satoshi Takemoto`,base_icon:'xlm.png',quote_icon:'btc.png'},
    {id:4,type:'crypto2crypto',code:'LTC/BTC',base:'litecoin',quote:'bitcoin',base_info:`Litecoin is a peer-to-peer cryptocurrency and open source software project released under the MIT/X11 license. Creation and transfer of coins is based on an open source cryptographic protocol and is not managed by any central authority. Litecoin was an early bitcoin spinoff or altcoin, starting in October 2011.
    Original author(s): Charlie Lee
    Initial release: 0.1.0 / 7 October 2011
    Forked from: Bitcoin
    Supply limit: 84,000,000 LTC
    Circulating supply: 58,414,406 LTC (23 September 2018)`,quote_info:`Bitcoin is a decentralized digital currency without a central bank or single administrator that can be sent from user-to-user on the peer-to-peer bitcoin network without the need for intermediaries.
    Initial release: 0.1.0 / 9 January 2009 
    Coins: Unspent outputs of transactions (in multiples of a satoshi)
    Supply limit: 21,000,000
    Founder: Satoshi Takemoto`,base_icon:'ltc.png',quote_icon:'btc.png'},
    {id:5,type:'crypto2crypto',code:'ADA/BTC',base:'cardano',quote:'bitcoin',base_info:`Cardano (ADA) is a decentralized block platform on opensource smart contracts, launched in September 2017 by Blockchain Development Output Hong Kong (IOHK).
    Cardano has a current market cap of $992.41M and has grown by 1500% since September 2017
    
    Original author(s): Charles Hoskinson
    
    Initial release: September 2017
    
    Circulating supply: 25,927,070,538 ADA
    
    Hash function: SHA256`,quote_info:`Bitcoin is a decentralized digital currency without a central bank or single administrator that can be sent from user-to-user on the peer-to-peer bitcoin network without the need for intermediaries.
    Initial release: 0.1.0 / 9 January 2009 
    Coins: Unspent outputs of transactions (in multiples of a satoshi)
    Supply limit: 21,000,000
    Founder: Satoshi Takemoto`,base_icon:'ada.png',quote_icon:'btc.png'},
    {id:6,type:'crypto2crypto',code:'XMR/BTC',base:'monero',quote:'bitcoin',base_info:`Monero is an open-source cryptocurrency created in April 2014 that focuses on fungibility and decentralization. Monero uses an obfuscated public ledger, meaning anybody can broadcast or send transactions, but no outside observer can tell the source, amount or destination.
    Original author(s): Nicolas van Saberhagen
    Hash function: CryptoNight
    Circulating supply: 16,250,168 XMR (as of 22 July 2018)
    Initial release date: 2014`,quote_info:`Bitcoin is a decentralized digital currency without a central bank or single administrator that can be sent from user-to-user on the peer-to-peer bitcoin network without the need for intermediaries.
    Initial release: 0.1.0 / 9 January 2009 
    Coins: Unspent outputs of transactions (in multiples of a satoshi)
    Supply limit: 21,000,000
    Founder: Satoshi Takemoto`,base_icon:'xmr.png',quote_icon:'btc.png'},
    {id:7,type:'crypto2crypto',code:'TRX/BTC',base:'tron',quote:'bitcoin',base_info:`TRON is a blockchain-based, decentralized protocol project with an internal TRON (TRX) coin that aims to be a content distribution platform for the digital entertainment industry. On June 2018, TRON’s team launched of its mainnet, in other words its own proprietary blockchain, to which it migrated all the TRX (ERC-20) tokens that previously circulated on the Ethereum blockchain. 
    Original author(s): Justin Sun 
    Hash function: Lamport algorithm
    Circulating supply: Demands of 100 Billion 
    Initial release date: Sep 2017`,quote_info:`Bitcoin is a decentralized digital currency without a central bank or single administrator that can be sent from user-to-user on the peer-to-peer bitcoin network without the need for intermediaries.
    Initial release: 0.1.0 / 9 January 2009 
    Coins: Unspent outputs of transactions (in multiples of a satoshi)
    Supply limit: 21,000,000
    Founder: Satoshi Takemoto`,base_icon:'trx.png',quote_icon:'btc.png'},
    {id:8,type:'crypto2crypto',code:'NEO/BTC',base:'neo',quote:'bitcoin',base_info:`NEO is a blockchain platform and cryptocurrency designed to build a scalable network of decentralized applications. The base asset of the NEO blockchain is the non-divisible NEO token which generates GAS tokens that can be used to pay for transaction fees generated by applications on the network.
    Initial release: February 2014 as AntShares
    Original author(s): Da Hongfei, Erik Zhang
    Circulating supply: c. 65.0 million (as of 6 March 2018)
    Supply limit: 100 million`,quote_info:`Bitcoin is a decentralized digital currency without a central bank or single administrator that can be sent from user-to-user on the peer-to-peer bitcoin network without the need for intermediaries.
    Initial release: 0.1.0 / 9 January 2009 
    Coins: Unspent outputs of transactions (in multiples of a satoshi)
    Supply limit: 21,000,000
    Founder: Satoshi Takemoto`,base_icon:'neo.png',quote_icon:'btc.png'},
    {id:9,type:'crypto2crypto',code:'BNB/BTC',base:'binancecoin',quote:'bitcoin',base_info:`Binance Coin (BNB) is an ERC20 token issued on the Ethereum blockchain by the popular cryptocurrency exchange Binance. With this cryptocurrency, you can pay a сommission for transactions on the exchange. And if you decide to do so, you will receive additional discounts. In the first year, you will be able to get a discount – 50%, in the second – 25%, in the third – 12.5%, in the fourth – 6.75%.
    Original author(s): Binance
    Circulating Supply: 117,443,301 BNB
    Initial release date: July 1st, 2017
    Hash function: SHA256`,quote_info:`Bitcoin is a decentralized digital currency without a central bank or single administrator that can be sent from user-to-user on the peer-to-peer bitcoin network without the need for intermediaries.
    Initial release: 0.1.0 / 9 January 2009 
    Coins: Unspent outputs of transactions (in multiples of a satoshi)
    Supply limit: 21,000,000
    Founder: Satoshi Takemoto`,base_icon:'bnb.png',quote_icon:'btc.png'},
    {id:10,type:'crypto2crypto',code:'QTUM/BTC',base:'qtum',quote:'bitcoin',base_info:`Qtum is a Chinese hybrid platform that connects the existing blockchain with a virtual machine, such as Ethereum. In the Qtum blockchain, there is an internal token – Qtum coin. It is a platform that enables developers to build applications and smart contracts on the current blockchain technology.
    Original author(s): The Qtum Foundation
    Hash function: UTXO
    Circulating supply: 88,943,944 QTUM
    Initial release date: April 15, 2017`,quote_info:`Bitcoin is a decentralized digital currency without a central bank or single administrator that can be sent from user-to-user on the peer-to-peer bitcoin network without the need for intermediaries.
    Initial release: 0.1.0 / 9 January 2009 
    Coins: Unspent outputs of transactions (in multiples of a satoshi)
    Supply limit: 21,000,000
    Founder: Satoshi Takemoto`,base_icon:'qtum.png',quote_icon:'btc.png'},
    {id:11,type:'crypto2crypto',code:'ETC/BTC',base:'ethereum-classic',quote:'bitcoin',base_info:`Ethereum Classic is an open-source, public, blockchain-based distributed computing platform featuring smart contract functionality. It provides a decentralized Turing-complete virtual machine, the Ethereum Virtual Machine, which can execute scripts using an international network of public nodes.
    Original author(s): Vitalik Buterin
    Initial release date: July 30, 2015
    Market cap: 1.1 Billion
    Hash function: Ethash`,quote_info:`Bitcoin is a decentralized digital currency without a central bank or single administrator that can be sent from user-to-user on the peer-to-peer bitcoin network without the need for intermediaries.
    Initial release: 0.1.0 / 9 January 2009 
    Coins: Unspent outputs of transactions (in multiples of a satoshi)
    Supply limit: 21,000,000
    Founder: Satoshi Takemoto`,base_icon:'etc.png',quote_icon:'btc.png'},
    {id:12,type:'crypto2crypto',code:'STEEM/BTC',base:'steem',quote:'bitcoin',base_info:`Steem is a cryptocurrency used to power the platform Steemit - an incentivized blockchain social media platform. Users create and curate content on Steemit just like other social news platforms (e.g. Reddit, Hacker News) and get rewarded in Steem for their work.
    Original author(s): Steemit, Inc
    Circulating Supply: 279,795,344 STEEM
    Initial release date: July 4, 2016
    Hash function: SHA-256`,quote_info:`Bitcoin is a decentralized digital currency without a central bank or single administrator that can be sent from user-to-user on the peer-to-peer bitcoin network without the need for intermediaries.
    Initial release: 0.1.0 / 9 January 2009 
    Coins: Unspent outputs of transactions (in multiples of a satoshi)
    Supply limit: 21,000,000
    Founder: Satoshi Takemoto`,base_icon:'steem.png',quote_icon:'btc.png'},
    {id:13,type:'crypto2crypto',code:'DOGE/BTC',base:'dogecoin',quote:'bitcoin',base_info:`Dogecoin (DOGE) is a cryptocurrency featuring a likeness of the Shiba Inu dog from the "Doge" Internet meme as its logo. Introduced as a "joke currency" on 6 December 2013, Dogecoin quickly developed its own online community and reached a capitalization of US$60 million in January 2014.
    Original author(s): Billy Markus, Jackson Palmer
    Initial release: December 6, 2013
    Circulating supply: 113 billion 
    Hash function: Scrypt-based`,quote_info:`Bitcoin is a decentralized digital currency without a central bank or single administrator that can be sent from user-to-user on the peer-to-peer bitcoin network without the need for intermediaries.
    Initial release: 0.1.0 / 9 January 2009 
    Coins: Unspent outputs of transactions (in multiples of a satoshi)
    Supply limit: 21,000,000
    Founder: Satoshi Takemoto`,base_icon:'doge.png',quote_icon:'btc.png'},
    {id:14,type:'crypto2crypto',code:'WAX/BTC',base:'wax',quote:'bitcoin',base_info:`WAX is a decentralized platform that enables anyone to operate a fully functioning virtual marketplace with zero investment in security, infrastructure, or payment processing. Developed by the founders of OPSkins, the world’s leading marketplace for online video game assets, WAX coin is designed to serve the 400+ million online players who already collect, buy and sell in-game items.
    Original author(s): OPSkins
    Initial release: 2017
    Circulating supply: 934,793,407 WAX
    Hash function: DPoS`,quote_info:`Bitcoin is a decentralized digital currency without a central bank or single administrator that can be sent from user-to-user on the peer-to-peer bitcoin network without the need for intermediaries.
    Initial release: 0.1.0 / 9 January 2009 
    Coins: Unspent outputs of transactions (in multiples of a satoshi)
    Supply limit: 21,000,000
    Founder: Satoshi Takemoto`,base_icon:'wax.png',quote_icon:'btc.png'},
    //{id:15,type:'crypto2crypto',code:'BTCD/BTC',base:'dfg',quote:'bitcoin'},
    {id:16,type:'crypto2crypto',code:'ZEC/BTC',base:'zcash',quote:'bitcoin',base_info:`Zcash is a cryptocurrency aimed at using cryptography to provide enhanced privacy for its users compared to other cryptocurrencies such as Bitcoin. The Zerocoin protocol was improved and transformed into the Zerocash system, which was then developed into the Zcash cryptocurrency in 2016.
    Original author(s): Zerocoin Electric Coin Company
    Initial release date: October 28, 2016
    Circulating Supply: 4,960,806 ZEC
    Hash function: 501(c)3`,quote_info:`Bitcoin is a decentralized digital currency without a central bank or single administrator that can be sent from user-to-user on the peer-to-peer bitcoin network without the need for intermediaries.
    Initial release: 0.1.0 / 9 January 2009 
    Coins: Unspent outputs of transactions (in multiples of a satoshi)
    Supply limit: 21,000,000
    Founder: Satoshi Takemoto`,base_icon:'zec.png',quote_icon:'btc.png'},
    {id:17,type:'crypto2crypto',code:'ETH/BTC',base:'ethereum',quote:'bitcoin',base_info:`Ethereum is an open-source, public, blockchain-based distributed computing platform and operating system featuring smart contract functionality. It supports a modified version of Nakamoto consensus via transaction-based state transitions.
    Initial release date: July 30, 2015
    Written in: Go, C++, Rust, Solidity
    Supply Limit: 21 million 
    Founder: Vitalik Buterin`,quote_info:`Bitcoin is a decentralized digital currency without a central bank or single administrator that can be sent from user-to-user on the peer-to-peer bitcoin network without the need for intermediaries.
    Initial release: 0.1.0 / 9 January 2009 
    Coins: Unspent outputs of transactions (in multiples of a satoshi)
    Supply limit: 21,000,000
    Founder: Satoshi Takemoto`,base_icon:'eth.png',quote_icon:'btc.png'},
    {id:18,type:'crypto2crypto',code:'XPD/BTC',base:'petrodollar',quote:'bitcoin',base_info:`PetroDollar (XPD) is a decentralized, blockchain-based cryptocurrency that is the world's first regulatory-compliant digital currency that is supported by independently certified oil and gas reserves. 
    Each PetroDollar token represents the net value of one reserve barrel of crude oil or its natural gas equivalent.
    XPD has been developed with the central goal of becoming the world’s leading safe-haven tokenized store of value.
    
    Circulating Supply: 63,993,275 XPD
    
    Founder: Signal Capital Management`,quote_info:`Bitcoin is a decentralized digital currency without a central bank or single administrator that can be sent from user-to-user on the peer-to-peer bitcoin network without the need for intermediaries.
    Initial release: 0.1.0 / 9 January 2009 
    Coins: Unspent outputs of transactions (in multiples of a satoshi)
    Supply limit: 21,000,000
    Founder: Satoshi Takemoto`,base_icon:'xpd.png',quote_icon:'btc.png'},
    {id:19,type:'crypto2crypto',code:'XRP/ETH',base:'ripple',quote:'ethereum',base_info:`Ripple is a real-time gross settlement system, currency exchange and remittance network created by Ripple Labs Inc., a US-based technology company.
    Developed by: Ripple Labs
    Written in: C++
    Supply Limit: Still in production 
    Founders: Chris Larsen, Jed McCaleb`,quote_info:`Ethereum is an open-source, public, blockchain-based distributed computing platform and operating system featuring smart contract functionality. It supports a modified version of Nakamoto consensus via transaction-based state transitions.
    Initial release date: July 30, 2015
    Written in: Go, C++, Rust, Solidity
    Supply Limit: 21 million 
    Founder: Vitalik Buterin`,base_icon:'ripple.png',quote_icon:'eth.png'},
    {id:20,type:'crypto2crypto',code:'XRP/BCH',base:'ripple',quote:'bitcoin-cash',base_info:`Ripple is a real-time gross settlement system, currency exchange and remittance network created by Ripple Labs Inc., a US-based technology company.
    Developed by: Ripple Labs
    Written in: C++
    Supply Limit: Still in production 
    Founders: Chris Larsen, Jed McCaleb`,quote_info:`Bitcoin Cash is a cryptocurrency. In mid-2017, a group of developers wanting to increase bitcoin's block size limit prepared a code change. The change, called a hard fork, took effect on 1 August 2017. As a result, the bitcoin ledger called the blockchain and the cryptocurrency split in two.'ADA', 'cardano'
 
    First block after split (block #478559): 1 August 2017 
    Block #1: 9 January 2009 
    Block reward: 12.5 BCH
    Genesis block: 3 January 2009 
    Coins: Unspent outputs of transactions
    Supply limit: 21,000,000 BCH`,base_icon:'ripple.png',quote_icon:'bch.png'},
    {id:21,type:'crypto2crypto',code:'XRP/ADA',base:'ripple',quote:'cardano',base_info:`Ripple is a real-time gross settlement system, currency exchange and remittance network created by Ripple Labs Inc., a US-based technology company.
    Developed by: Ripple Labs
    Written in: C++
    Supply Limit: Still in production 
    Founders: Chris Larsen, Jed McCaleb`,quote_info:`Cardano (ADA) is a decentralized block platform on opensource smart contracts, launched in September 2017 by Blockchain Development Output Hong Kong (IOHK).
    Cardano has a current market cap of $992.41M and has grown by 1500% since September 2017
    
    Original author(s): Charles Hoskinson
    
    Initial release: September 2017
    
    Circulating supply: 25,927,070,538 ADA
    
    Hash function: SHA256`,base_icon:'ripple.png',quote_icon:'ada.png'},
    {id:22,type:'crypto2crypto',code:'XRP/LTC',base:'ripple',quote:'litecoin',base_info:`Ripple is a real-time gross settlement system, currency exchange and remittance network created by Ripple Labs Inc., a US-based technology company.
    Developed by: Ripple Labs
    Written in: C++
    Supply Limit: Still in production 
    Founders: Chris Larsen, Jed McCaleb`,quote_info:`Litecoin is a peer-to-peer cryptocurrency and open source software project released under the MIT/X11 license. Creation and transfer of coins is based on an open source cryptographic protocol and is not managed by any central authority. Litecoin was an early bitcoin spinoff or altcoin, starting in October 2011.
    Original author(s): Charlie Lee
    Initial release: 0.1.0 / 7 October 2011
    Forked from: Bitcoin
    Supply limit: 84,000,000 LTC
    Circulating supply: 58,414,406 LTC (23 September 2018)`,base_icon:'ripple.png',quote_icon:'ltc.png'},
    {id:23,type:'crypto2crypto',code:'ETC/ETH',base:'ethereum-classic',quote:'ethereum',base_info:`Ethereum Classic is an open-source, public, blockchain-based distributed computing platform featuring smart contract functionality. It provides a decentralized Turing-complete virtual machine, the Ethereum Virtual Machine, which can execute scripts using an international network of public nodes.
    Original author(s): Vitalik Buterin
    Initial release date: July 30, 2015
    Market cap: 1.1 Billion
    Hash function: Ethash`,quote_info:`Ethereum is an open-source, public, blockchain-based distributed computing platform and operating system featuring smart contract functionality. It supports a modified version of Nakamoto consensus via transaction-based state transitions.
    Initial release date: July 30, 2015
    Written in: Go, C++, Rust, Solidity
    Supply Limit: 21 million 
    Founder: Vitalik Buterin`,base_icon:'etc.png',quote_icon:'eth.png'},
    {id:24,type:'crypto2crypto',code:'NEO/ETH',base:'neo',quote:'ethereum',base_info:`NEO is a blockchain platform and cryptocurrency designed to build a scalable network of decentralized applications. The base asset of the NEO blockchain is the non-divisible NEO token which generates GAS tokens that can be used to pay for transaction fees generated by applications on the network.
    Initial release: February 2014 as AntShares
    Original author(s): Da Hongfei, Erik Zhang
    Circulating supply: c. 65.0 million (as of 6 March 2018)
    Supply limit: 100 million`,quote_info:`Ethereum is an open-source, public, blockchain-based distributed computing platform and operating system featuring smart contract functionality. It supports a modified version of Nakamoto consensus via transaction-based state transitions.
    Initial release date: July 30, 2015
    Written in: Go, C++, Rust, Solidity
    Supply Limit: 21 million 
    Founder: Vitalik Buterin`,base_icon:'neo.png',quote_icon:'eth.png'},
    {id:25,type:'crypto2crypto',code:'ADA/ETH',base:'cardano',quote:'ethereum',base_info:`Cardano (ADA) is a decentralized block platform on opensource smart contracts, launched in September 2017 by Blockchain Development Output Hong Kong (IOHK).
    Cardano has a current market cap of $992.41M and has grown by 1500% since September 2017
    
    Original author(s): Charles Hoskinson
    
    Initial release: September 2017
    
    Circulating supply: 25,927,070,538 ADA
    
    Hash function: SHA256`,quote_info:`Ethereum is an open-source, public, blockchain-based distributed computing platform and operating system featuring smart contract functionality. It supports a modified version of Nakamoto consensus via transaction-based state transitions.
    Initial release date: July 30, 2015
    Written in: Go, C++, Rust, Solidity
    Supply Limit: 21 million 
    Founder: Vitalik Buterin`,base_icon:'ada.png',quote_icon:'eth.png'},
    {id:26,type:'crypto2crypto',code:'XMR/ZEC',base:'monero',quote:'zcash',base_info:`Monero is an open-source cryptocurrency created in April 2014 that focuses on fungibility and decentralization. Monero uses an obfuscated public ledger, meaning anybody can broadcast or send transactions, but no outside observer can tell the source, amount or destination.
    Original author(s): Nicolas van Saberhagen
    Hash function: CryptoNight
    Circulating supply: 16,250,168 XMR (as of 22 July 2018)
    Initial release date: 2014`,quote_info:`Zcash is a cryptocurrency aimed at using cryptography to provide enhanced privacy for its users compared to other cryptocurrencies such as Bitcoin. The Zerocoin protocol was improved and transformed into the Zerocash system, which was then developed into the Zcash cryptocurrency in 2016.
    Original author(s): Zerocoin Electric Coin Company
    Initial release date: October 28, 2016
    Circulating Supply: 4,960,806 ZEC
    Hash function: 501(c)3`,base_icon:'xmr.png',quote_icon:'zec.png'},
    {id:33,type:'crypto2fiat',code:'BTC/EUR',base:'bitcoin',quote:'eur',base_info:`Bitcoin is a decentralized digital currency without a central bank or single administrator that can be sent from user-to-user on the peer-to-peer bitcoin network without the need for intermediaries.
    Initial release: 0.1.0 / 9 January 2009 
    Coins: Unspent outputs of transactions (in multiples of a satoshi)
    Supply limit: 21,000,000
    Founder: Satoshi Takemoto`,quote_info:`The Euro (EUR) is the official currency of 19 of 28 member states of the European Union, as well as some of the territories of the EU. 
    Since its launch 1999, the Euro is known to have rises and set backs but has proven its worth and is now the second most traded currency in the Forex Market.  `,base_icon:'btc.png',quote_icon:'eur.png'},
    {id:34,type:'crypto2fiat',code:'BTC/GBP',base:'bitcoin',quote:'gbp',base_info:`Bitcoin is a decentralized digital currency without a central bank or single administrator that can be sent from user-to-user on the peer-to-peer bitcoin network without the need for intermediaries.
    Initial release: 0.1.0 / 9 January 2009 
    Coins: Unspent outputs of transactions (in multiples of a satoshi)
    Supply limit: 21,000,000
    Founder: Satoshi Takemoto`,quote_info:`The Pound Sterling (GBP), commonly known as the pound and less commonly referred to as Sterling, is the official currency of the United Kingdom and it's territories. 
    The GBP was considered one the most precious currencies in the world, however due to Brexit has shown some signs of volatility. `,base_icon:'btc.png',quote_icon:'gbp.png'},
    {id:35,type:'crypto2fiat',code:'BTC/USD',base:'bitcoin',quote:'usd',base_info:`Bitcoin is a decentralized digital currency without a central bank or single administrator that can be sent from user-to-user on the peer-to-peer bitcoin network without the need for intermediaries.
    Initial release: 0.1.0 / 9 January 2009 
    Coins: Unspent outputs of transactions (in multiples of a satoshi)
    Supply limit: 21,000,000
    Founder: Satoshi Takemoto`,quote_info:`The United States Dollar (USD) is the official currency of the United States and its territories per the United States Constitution since 1792. The USD holds a key position in the global economy serving as a quote coin to almost any traded asset.`,base_icon:'btc.png',quote_icon:'usd.png'},
    {id:36,type:'crypto2fiat',code:'XRP/EUR',base:'ripple',quote:'eur',base_info:`Ripple is a real-time gross settlement system, currency exchange and remittance network created by Ripple Labs Inc., a US-based technology company.
    Developed by: Ripple Labs
    Written in: C++
    Supply Limit: Still in production 
    Founders: Chris Larsen, Jed McCaleb`,quote_info:`The Euro (EUR) is the official currency of 19 of 28 member states of the European Union, as well as some of the territories of the EU. 
    Since its launch 1999, the Euro is known to have rises and set backs but has proven its worth and is now the second most traded currency in the Forex Market.  `,base_icon:'ripple.png',quote_icon:'eur.png'},
    {id:37,type:'crypto2fiat',code:'XRP/GBP',base:'ripple',quote:'gbp',base_info:`Ripple is a real-time gross settlement system, currency exchange and remittance network created by Ripple Labs Inc., a US-based technology company.
    Developed by: Ripple Labs
    Written in: C++
    Supply Limit: Still in production 
    Founders: Chris Larsen, Jed McCaleb`,quote_info:`The Pound Sterling (GBP), commonly known as the pound and less commonly referred to as Sterling, is the official currency of the United Kingdom and it's territories. 
    The GBP was considered one the most precious currencies in the world, however due to Brexit has shown some signs of volatility. `,base_icon:'ripple.png',quote_icon:'gbp.png'},
    {id:38,type:'crypto2fiat',code:'XRP/USD',base:'ripple',quote:'usd',base_info:`Ripple is a real-time gross settlement system, currency exchange and remittance network created by Ripple Labs Inc., a US-based technology company.
    Developed by: Ripple Labs
    Written in: C++
    Supply Limit: Still in production 
    Founders: Chris Larsen, Jed McCaleb`,quote_info:`The United States Dollar (USD) is the official currency of the United States and its territories per the United States Constitution since 1792. The USD holds a key position in the global economy serving as a quote coin to almost any traded asset.`,base_icon:'ripple.png',quote_icon:'usd.png'},
    {id:39,type:'crypto2fiat',code:'ETC/EUR',base:'ethereum-classic',quote:'eur',base_info:`Ethereum Classic is an open-source, public, blockchain-based distributed computing platform featuring smart contract functionality. It provides a decentralized Turing-complete virtual machine, the Ethereum Virtual Machine, which can execute scripts using an international network of public nodes.
    Original author(s): Vitalik Buterin
    Initial release date: July 30, 2015
    Market cap: 1.1 Billion
    Hash function: Ethash`,quote_info:`The Euro (EUR) is the official currency of 19 of 28 member states of the European Union, as well as some of the territories of the EU. 
    Since its launch 1999, the Euro is known to have rises and set backs but has proven its worth and is now the second most traded currency in the Forex Market.  `,base_icon:'etc.png',quote_icon:'eur.png'},
    {id:40,type:'crypto2fiat',code:'ETC/GBP',base:'ethereum-classic',quote:'gbp',base_info:`Ethereum Classic is an open-source, public, blockchain-based distributed computing platform featuring smart contract functionality. It provides a decentralized Turing-complete virtual machine, the Ethereum Virtual Machine, which can execute scripts using an international network of public nodes.
    Original author(s): Vitalik Buterin
    Initial release date: July 30, 2015
    Market cap: 1.1 Billion
    Hash function: Ethash`,quote_info:`The Pound Sterling (GBP), commonly known as the pound and less commonly referred to as Sterling, is the official currency of the United Kingdom and it's territories. 
    The GBP was considered one the most precious currencies in the world, however due to Brexit has shown some signs of volatility. `,base_icon:'etc.png',quote_icon:'gbp.png'},
    {id:41,type:'crypto2fiat',code:'ETC/USD',base:'ethereum-classic',quote:'usd',base_info:`Ethereum Classic is an open-source, public, blockchain-based distributed computing platform featuring smart contract functionality. It provides a decentralized Turing-complete virtual machine, the Ethereum Virtual Machine, which can execute scripts using an international network of public nodes.
    Original author(s): Vitalik Buterin
    Initial release date: July 30, 2015
    Market cap: 1.1 Billion
    Hash function: Ethash`,quote_info:`The United States Dollar (USD) is the official currency of the United States and its territories per the United States Constitution since 1792. The USD holds a key position in the global economy serving as a quote coin to almost any traded asset.`,base_icon:'etc.png',quote_icon:'usd.png'},
    {id:42,type:'crypto2fiat',code:'NEO/EUR',base:'neo',quote:'eur',base_info:`NEO is a blockchain platform and cryptocurrency designed to build a scalable network of decentralized applications. The base asset of the NEO blockchain is the non-divisible NEO token which generates GAS tokens that can be used to pay for transaction fees generated by applications on the network.
    Initial release: February 2014 as AntShares
    Original author(s): Da Hongfei, Erik Zhang
    Circulating supply: c. 65.0 million (as of 6 March 2018)
    Supply limit: 100 million`,quote_info:`The Euro (EUR) is the official currency of 19 of 28 member states of the European Union, as well as some of the territories of the EU. 
    Since its launch 1999, the Euro is known to have rises and set backs but has proven its worth and is now the second most traded currency in the Forex Market.  `,base_icon:'neo.png',quote_icon:'eur.png'},
    {id:43,type:'crypto2fiat',code:'NEO/GBP',base:'neo',quote:'gbp',base_info:`NEO is a blockchain platform and cryptocurrency designed to build a scalable network of decentralized applications. The base asset of the NEO blockchain is the non-divisible NEO token which generates GAS tokens that can be used to pay for transaction fees generated by applications on the network.
    Initial release: February 2014 as AntShares
    Original author(s): Da Hongfei, Erik Zhang
    Circulating supply: c. 65.0 million (as of 6 March 2018)
    Supply limit: 100 million`,quote_info:`The Pound Sterling (GBP), commonly known as the pound and less commonly referred to as Sterling, is the official currency of the United Kingdom and it's territories. 
    The GBP was considered one the most precious currencies in the world, however due to Brexit has shown some signs of volatility. `,base_icon:'neo.png',quote_icon:'gbp.png'},
    {id:44,type:'crypto2fiat',code:'NEO/USD',base:'neo',quote:'usd',base_info:`NEO is a blockchain platform and cryptocurrency designed to build a scalable network of decentralized applications. The base asset of the NEO blockchain is the non-divisible NEO token which generates GAS tokens that can be used to pay for transaction fees generated by applications on the network.
    Initial release: February 2014 as AntShares
    Original author(s): Da Hongfei, Erik Zhang
    Circulating supply: c. 65.0 million (as of 6 March 2018)
    Supply limit: 100 million`,quote_info:`The United States Dollar (USD) is the official currency of the United States and its territories per the United States Constitution since 1792. The USD holds a key position in the global economy serving as a quote coin to almost any traded asset.`,base_icon:'neo.png',quote_icon:'usd.png'},
    {id:45,type:'crypto2fiat',code:'ADA/EUR',base:'cardano',quote:'eur',base_info:`Cardano (ADA) is a decentralized block platform on opensource smart contracts, launched in September 2017 by Blockchain Development Output Hong Kong (IOHK).
    Cardano has a current market cap of $992.41M and has grown by 1500% since September 2017
    
    Original author(s): Charles Hoskinson
    
    Initial release: September 2017
    
    Circulating supply: 25,927,070,538 ADA
    
    Hash function: SHA256`,quote_info:`The Euro (EUR) is the official currency of 19 of 28 member states of the European Union, as well as some of the territories of the EU. 
    Since its launch 1999, the Euro is known to have rises and set backs but has proven its worth and is now the second most traded currency in the Forex Market.  `,base_icon:'ada.png',quote_icon:'eur.png'},
    {id:46,type:'crypto2fiat',code:'ADA/GBP',base:'cardano',quote:'gbp',base_info:`Cardano (ADA) is a decentralized block platform on opensource smart contracts, launched in September 2017 by Blockchain Development Output Hong Kong (IOHK).
    Cardano has a current market cap of $992.41M and has grown by 1500% since September 2017
    
    Original author(s): Charles Hoskinson
    
    Initial release: September 2017
    
    Circulating supply: 25,927,070,538 ADA
    
    Hash function: SHA256`,quote_info:`The Pound Sterling (GBP), commonly known as the pound and less commonly referred to as Sterling, is the official currency of the United Kingdom and it's territories. 
    The GBP was considered one the most precious currencies in the world, however due to Brexit has shown some signs of volatility. `,base_icon:'ada.png',quote_icon:'gbp.png'},
    {id:47,type:'crypto2fiat',code:'ADA/USD',base:'cardano',quote:'usd',base_info:`Cardano (ADA) is a decentralized block platform on opensource smart contracts, launched in September 2017 by Blockchain Development Output Hong Kong (IOHK).
    Cardano has a current market cap of $992.41M and has grown by 1500% since September 2017
    
    Original author(s): Charles Hoskinson
    
    Initial release: September 2017
    
    Circulating supply: 25,927,070,538 ADA
    
    Hash function: SHA256`,quote_info:`The United States Dollar (USD) is the official currency of the United States and its territories per the United States Constitution since 1792. The USD holds a key position in the global economy serving as a quote coin to almost any traded asset.`,base_icon:'ada.png',quote_icon:'usd.png'},
    {id:48,type:'crypto2fiat',code:'XMR/EUR',base:'monero',quote:'eur',base_info:`Monero is an open-source cryptocurrency created in April 2014 that focuses on fungibility and decentralization. Monero uses an obfuscated public ledger, meaning anybody can broadcast or send transactions, but no outside observer can tell the source, amount or destination.
    Original author(s): Nicolas van Saberhagen
    Hash function: CryptoNight
    Circulating supply: 16,250,168 XMR (as of 22 July 2018)
    Initial release date: 2014`,quote_info:`The Euro (EUR) is the official currency of 19 of 28 member states of the European Union, as well as some of the territories of the EU. 
    Since its launch 1999, the Euro is known to have rises and set backs but has proven its worth and is now the second most traded currency in the Forex Market.  `,base_icon:'xmr.png',quote_icon:'eur.png'},
    {id:49,type:'crypto2fiat',code:'XMR/GBP',base:'monero',quote:'gbp',base_info:`Monero is an open-source cryptocurrency created in April 2014 that focuses on fungibility and decentralization. Monero uses an obfuscated public ledger, meaning anybody can broadcast or send transactions, but no outside observer can tell the source, amount or destination.
    Original author(s): Nicolas van Saberhagen
    Hash function: CryptoNight
    Circulating supply: 16,250,168 XMR (as of 22 July 2018)
    Initial release date: 2014`,quote_info:`The Pound Sterling (GBP), commonly known as the pound and less commonly referred to as Sterling, is the official currency of the United Kingdom and it's territories. 
    The GBP was considered one the most precious currencies in the world, however due to Brexit has shown some signs of volatility. `,base_icon:'xmr.png',quote_icon:'gbp.png'},
    {id:50,type:'crypto2fiat',code:'XMR/USD',base:'monero',quote:'usd',base_info:`Monero is an open-source cryptocurrency created in April 2014 that focuses on fungibility and decentralization. Monero uses an obfuscated public ledger, meaning anybody can broadcast or send transactions, but no outside observer can tell the source, amount or destination.
    Original author(s): Nicolas van Saberhagen
    Hash function: CryptoNight
    Circulating supply: 16,250,168 XMR (as of 22 July 2018)
    Initial release date: 2014`,quote_info:`The United States Dollar (USD) is the official currency of the United States and its territories per the United States Constitution since 1792. The USD holds a key position in the global economy serving as a quote coin to almost any traded asset.`,base_icon:'xmr.png',quote_icon:'usd.png'},
    {id:51,type:'crypto2fiat',code:'LTC/EUR',base:'litecoin',quote:'eur',base_info:`Litecoin is a peer-to-peer cryptocurrency and open source software project released under the MIT/X11 license. Creation and transfer of coins is based on an open source cryptographic protocol and is not managed by any central authority. Litecoin was an early bitcoin spinoff or altcoin, starting in October 2011.
    Original author(s): Charlie Lee
    Initial release: 0.1.0 / 7 October 2011
    Forked from: Bitcoin
    Supply limit: 84,000,000 LTC
    Circulating supply: 58,414,406 LTC (23 September 2018)`,quote_info:`The Euro (EUR) is the official currency of 19 of 28 member states of the European Union, as well as some of the territories of the EU. 
    Since its launch 1999, the Euro is known to have rises and set backs but has proven its worth and is now the second most traded currency in the Forex Market.  `,base_icon:'ltc.png',quote_icon:'eur.png'},
    {id:52,type:'crypto2fiat',code:'LTC/GBP',base:'litecoin',quote:'gbp',base_info:`Litecoin is a peer-to-peer cryptocurrency and open source software project released under the MIT/X11 license. Creation and transfer of coins is based on an open source cryptographic protocol and is not managed by any central authority. Litecoin was an early bitcoin spinoff or altcoin, starting in October 2011.
    Original author(s): Charlie Lee
    Initial release: 0.1.0 / 7 October 2011
    Forked from: Bitcoin
    Supply limit: 84,000,000 LTC
    Circulating supply: 58,414,406 LTC (23 September 2018)`,quote_info:`The Pound Sterling (GBP), commonly known as the pound and less commonly referred to as Sterling, is the official currency of the United Kingdom and it's territories. 
    The GBP was considered one the most precious currencies in the world, however due to Brexit has shown some signs of volatility. `,base_icon:'ltc.png',quote_icon:'gbp.png'},
    {id:53,type:'crypto2fiat',code:'LTC/USD',base:'litecoin',quote:'usd',base_info:`Litecoin is a peer-to-peer cryptocurrency and open source software project released under the MIT/X11 license. Creation and transfer of coins is based on an open source cryptographic protocol and is not managed by any central authority. Litecoin was an early bitcoin spinoff or altcoin, starting in October 2011.
    Original author(s): Charlie Lee
    Initial release: 0.1.0 / 7 October 2011
    Forked from: Bitcoin
    Supply limit: 84,000,000 LTC
    Circulating supply: 58,414,406 LTC (23 September 2018)`,quote_info:`The United States Dollar (USD) is the official currency of the United States and its territories per the United States Constitution since 1792. The USD holds a key position in the global economy serving as a quote coin to almost any traded asset.`,base_icon:'ltc.png',quote_icon:'usd.png'},
    //{id:27,type:'',code:'Crude Oil WTI/BTC'},Oil Pact Chain (OPC)
   /* {id:28,type:'',code:'Silver/BTC'},
    {id:29,type:'',code:'Gold/BTC'},
    {id:30,type:'etf',code:'DG-Gold ETF'},
    {id:31,type:'etf',code:'LVB ETF'},
    {id:32,type:'etf',code:'Petrol ETF'},*/

];


module.exports = AssetsList;
