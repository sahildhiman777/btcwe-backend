class UsdRates {
    constructor(){
        this.rates = {}
    }
    setRates(rates){
        this.rates = rates
    }
    getRateByCoin(name){
        if(this.rates[name] == undefined)
            return 0;
        return this.rates[name].usd
    }
    getRates(){
        return this.rates
    }
}

module.exports = UsdRates;