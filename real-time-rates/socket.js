let connection = null;

class Socket {
    constructor() {
        this._socket = null;
    }
    connect(server,sessionMiddleware) {
        this._socket = require('socket.io')(server); 
        /*
        io.use(function(socket, next) {
            sessionMiddleware(socket.request, socket.request.res, next);
        });
        */
            this._socket.on('connection',(data)=>{
                console.log(`New socket connection: ${data.id}`);
            });

            this._socket.on('disconnect', function () {
                console.log(socket.id,"Un socket se desconecto");
            });

            
    }

    sendEvent(event, data) {
        this._socket.emit(event, data);
    }

    registerEvent(event, handler) {
        this._socket.on(event, handler);
    }

    static init(server,sessionMiddleware) {
        if(!connection) {
            connection = new Socket();
            connection.connect(server,sessionMiddleware);
        }
    }

    static getConnection() {
        if(!connection) {
            throw new Error("no active connection");
        }
        return connection;
    }
}

module.exports = {
    connect: Socket.init,
    connection: Socket.getConnection 
}