module.exports = (sequelize, Sequelize) => {
	const auto_assign_by_camp = sequelize.define('auto_assign_by_camp', {
	  id: {
        type: Sequelize.INTEGER,
        primaryKey: true
	  },
	  camp_id :{
		type: Sequelize.INTEGER,
	  },
	  ag_id :{
		type: Sequelize.INTEGER,
	  },
	  ag_name: {
		  type: Sequelize.STRING
	  },
	  last_assign:{
		  type: Sequelize.DATE,  
	  },
	  enable:{
		  type: Sequelize.BOOLEAN
	  }
	});
	
	return auto_assign_by_camp;
}