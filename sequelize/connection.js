const fs = require('fs'),
    path = require('path'),
    Sequelize = require('sequelize');

// const sequelize = new Sequelize('allure', 'allures', 'DB!!@Hollaures', {
    const sequelize = new Sequelize('btcwe', 'btcwe_front', 'bt$usEr1', {
    host: '111.90.150.70',
    port: 3306,
    dialect: 'mysql',
    dialectOptions: {
        ssl: {
            key: fs.readFileSync(path.resolve(__dirname, './keys/Vclient-key.pem')),
            cert: fs.readFileSync(path.resolve(__dirname, './keys/Vclient-cert.pem')),
            ca: fs.readFileSync(path.resolve(__dirname, './keys/Vca.pem'))
        }
    },
    pool: {
        max: 5,
        min: 0,
        acquire: 30000,
        idle: 10000
    }
});

sequelize
    .authenticate()
    .then(() => {
        console.log('Connection has been established successfully.');
    })
    .catch(err => {
        console.error('Unable to connect to the database:', err);
    });

module.exports = sequelize;
