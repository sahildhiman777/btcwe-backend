var fs = require('fs');
var path = require('path');

module.exports = function (data) {
    var contactUsTemplate = fs.readFileSync(path.join(__dirname, '/contactUsTemplate.html'), { encoding: 'utf-8' });
    contactUsTemplate = contactUsTemplate.toString();
    contactUsTemplate = contactUsTemplate.replace('#email', data.email);
    contactUsTemplate = contactUsTemplate.replace('#name', data.name);
    contactUsTemplate = contactUsTemplate.replace('#message', data.message);
;
    return contactUsTemplate;
}