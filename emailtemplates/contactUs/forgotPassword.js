var fs = require('fs');
var path = require('path');

module.exports = function (data) {
    var affiliateTemplate = fs.readFileSync(path.join(__dirname, '/forgotPassword.html'), { encoding: 'utf-8' });
    affiliateTemplate = affiliateTemplate.toString();
    affiliateTemplate = affiliateTemplate.replace('#email', data.email);
    affiliateTemplate = affiliateTemplate.replace('#name', data.name);
    affiliateTemplate = affiliateTemplate.replace('#token', data.token);
;
    return affiliateTemplate;
}