var fs = require('fs');
var path = require('path');

module.exports = function (data) {
    var wireTransfer = fs.readFileSync(path.join(__dirname, '/wireTransfer.html'), { encoding: 'utf-8' });
    wireTransfer = wireTransfer.toString();
    wireTransfer = wireTransfer.replace('#email', data.email);
    wireTransfer = wireTransfer.replace('#amount', data.amount);
;
    return wireTransfer;
}